<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 01/10/17
 * Time: 22:51
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Producto;
use Doctrine\ORM\EntityRepository;

class ProductosRepository extends EntityRepository
{
    public function obtenerTortas(){
        return $this->createQueryBuilder('producto')
            ->andWhere('producto.codigo LIKE :codigoProducto')
            ->setParameter('codigoProducto', '%TO%')
            ->getQuery()
            ->execute();
    }

    public function obtenerTartas(){
        return $this->createQueryBuilder('producto')
            ->andWhere('producto.codigo LIKE :codigoProducto')
            ->setParameter('codigoProducto', '%TA%')
            ->getQuery()
            ->execute();
    }
    public function obtenerMesasDulces(){
        return $this->createQueryBuilder('producto')
            ->andWhere('producto.codigo LIKE :codigoProducto')
            ->setParameter('codigoProducto', '%MD%')
            ->getQuery()
            ->execute();
    }
    public function obtenerVariedades(){
        return $this->createQueryBuilder('producto')
            ->andWhere('producto.codigo LIKE :codigoProducto')
            ->setParameter('codigoProducto', '%VA%')
            ->getQuery()
            ->execute();
    }


}