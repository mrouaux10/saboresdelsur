<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 21/09/17
 * Time: 18:56
 */

namespace AppBundle\Security;

use AppBundle\Form\loginForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $formFactory;
    private $em;
    private $router;
    private $passwordEncoder;

    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $em, RouterInterface $router, UserPasswordEncoderInterface $passwordEncoder)
    {

        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->getPathInfo() == '/login' && $request->isMethod('POST');
        if (!$isLoginSubmit){
            // si no esta iniciando sesion, retorno null y Symfony no pide credenciales y sigue normal.
            // Si retorna algo distinto a null, symfony llama al metodo 'getUser'.
            return;
        }
        // No puedo hacer $form = $this->createForm aca porque no es un Controlador,
        //por eso hago independency injection
        $form = $this->formFactory->create(loginForm::class);
        $form->handleRequest($request);
        $data = $form->getData();

        // (Este getSession es para mantener el email completado cuando el password es incorrecto).
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_username']
        );
        return $data;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['_username'];
        // Aca tmb tuve que hacer independency injection para buscar el user con el getRepository.
        return $this->em->getRepository('AppBundle:User')
            ->findOneBy(['email' => $username]);
        // Si esto no retorna null, Gard llama a CheckCredentials
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];

/*        if ($password == "tania" | $password == "agustina"){
            return true;
        }*/
        if ($this->passwordEncoder->isPasswordValid($user,$password)){
           return true;
        }

        return false;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('security_login');

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);

        if (!$targetPath) {
            $targetPath = $this->router->generate('admin_profile');
        }

        return new RedirectResponse($targetPath);
    }

}