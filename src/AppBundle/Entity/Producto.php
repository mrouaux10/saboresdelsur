<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 01/10/17
 * Time: 19:52
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\DateTime;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductosRepository")
 * @Vich\Uploadable()
 * @ORM\Table(name="producto")
 */
class Producto
{
    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(length=1500)
     */
    private $descripcion;
    /**
     * @ORM\Column(type="string")
     */
    private $codigo;
    /**
     * @ORM\Column(type="string")
     */
    private $tituloDescripcion;

    /**
     * @return mixed
     */
    public function getTituloDescripcion()
    {
        return $this->tituloDescripcion;
    }

    /**
     * @param mixed $tituloDescripcion
     */
    public function setTituloDescripcion($tituloDescripcion)
    {
        $this->tituloDescripcion = $tituloDescripcion;
    }
    /**
     * @ORM\Column(type="string")
     */
    private $categoria;

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }
    /**
     * @Vich\UploadableField(mapping="producto", fileNameProperty="image.name", size="image.size", mimeType="image.mimeType", originalName="image.originalName")
     * @var File
     */
    private $imageFile;
    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     * @var EmbeddedFile
     */
    private $image;
    /**
     * @return mixed
     */
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->image = new EmbeddedFile();
    }

    /**
     * @param File|UploadedFile $image
     */
    public function setImageFile(File $image = null){
        $this->imageFile = $image;
        if ($image){
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return File|null
     */
    public function getImageFile(){
        return $this->imageFile;
    }

    /**
     * @param EmbeddedFile $image
     */
    public function setImage(EmbeddedFile $image){
        $this->image = $image;
    }

    /**
     * @return EmbeddedFile
     */
    public function getImage(){
        return $this->image;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }
    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
}