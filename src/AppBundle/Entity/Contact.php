<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 18/12/17
 * Time: 16:36
 */

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, completa tu nombre.")
     */
    private $name;
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, completa tu email.")
     * @Assert\Email(message="El email no parece ser correcto.")
     */
    private $email;
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, coloca un nombre de tema.")
     */
    private $topic;
    /**
     * @var
     * @Assert\NotBlank(message="Déjanos un comentario sobre el motivo de tu interés en contactarte con nosotros.")
     */
    private $mensaje;

    /**
     * @param mixed $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @param mixed $mensaje
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }
    /**
     * @return mixed
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }
    public function getTopic()
    {
        return $this->topic;
    }
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
}