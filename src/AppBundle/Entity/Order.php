<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 08/10/17
 * Time: 16:06
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Order
{
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, completa tu nombre.")
     */
    private $name;
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, completa tu email.")
     * @Assert\Email(message="El email no parece ser correcto.")
     */
    private $email;
    /**
     * @var
     * @Assert\NotBlank(message="Por favor, completa tu teléfono.")
     */
    private $phone;
    /**
     * @var
     * @Assert\Date(message="El formato de fecha no es válido.")
     */
    private $dateExpected;
    /**
     * @var
     * @Assert\NotBlank(message="Primero debes seleccionar un producto de la galeria haciendo''click'' sobre él y seleccionando '' Esta es para mí ''.")
     */
    private $orderCode;
    /**
     * @var
     * @Assert\NotBlank(message="Coméntanos los detalles de como te gustaría este producto, (cantidad de pisos, ingredientes extra, y demás).")
     */
    private $descripcion;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getDateExpected()
    {
        return $this->dateExpected;
    }

    /**
     * @param mixed $dateExpected
     */
    public function setDateExpected($dateExpected)
    {
        $this->dateExpected = $dateExpected;
    }

    /**
     * @return mixed
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * @param mixed $orderCode
     */
    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
}