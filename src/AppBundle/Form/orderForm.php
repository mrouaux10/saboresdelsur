<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 26/09/17
 * Time: 23:21
 */

namespace AppBundle\Form;

use AppBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class orderForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', NumberType::class)
            ->add('orderCode', TextType::class, array(
                'attr' => array(
                    'class' => 'invisible',
                )
            ))
            ->add('descripcion', TextareaType::class)
            ->add('dateExpected', DateType::class, array(
                'widget' => 'single_text',
                'mapped' => false,
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'html5' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver ->setDefaults([
            'data_class' => 'AppBundle\Entity\Order'
        ]);
    }
}