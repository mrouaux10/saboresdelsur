<?php

namespace AppBundle\Form;

use Symfony\Component\Console\Descriptor\TextDescriptor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class uploadFileForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile',FileType::class)
            ->add('descripcion', TextareaType::class)
            ->add('tituloDescripcion')
            ->add('categoria', ChoiceType::class,array(
                'choices' => array(
                    'Torta' => 'Torta',
                    'Tarta' => 'Tarta',
                    'Mesa Dulce' => 'Mesa dulce',
                    'Variedad' => 'Variedad',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Producto'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundleupload_file_form';
    }
}
