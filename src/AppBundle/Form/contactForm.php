<?php

namespace AppBundle\Form;

use AppBundle\AppBundle;
use AppBundle\Entity\Order;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class contactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email', EmailType::class)
            ->add('topic')
            ->add('mensaje', TextareaType::class);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver ->setDefaults([
            'data_class' => 'AppBundle\Entity\Contact'
        ]);
    }
}
