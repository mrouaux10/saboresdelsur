<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 22/09/17
 * Time: 20:26
 */

namespace AppBundle\Doctrine;


use AppBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\Tests\Debug\EventSubscriber;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class HashPasswordListener implements \Doctrine\Common\EventSubscriber
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function prePersist(LifecycleEventArgs $args){
        // Obtengo la entidad que es guardada
        $entity = $args->getEntity();
        if (!$entity instanceof User){
            return;
        }

        $this->encodePassword($entity);
    }

    public function preUpdate(LifecycleEventArgs $args){
        // Obtengo la entidad que es guardada
        $entity = $args->getEntity();
        if (!$entity instanceof User || !$entity->getPlainPassword()){
            return;
        }


        $this->encodePassword($entity);
        // necessary to force the update to see the change
        $em = $args->getEntityManager();
        $meta = $em->getClassMetadata(get_class($entity));
        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $entity);
    }

    public function getSubscribedEvents()
    {
        //prePersist se llama antes de que se inserte la entidad.
        //preUpdate se llama antes de que se actualice la entidad.
        return ['prePersist', 'preUpdate'];
    }


    /**
     * @param User $entity
     */
    public function encodePassword(User $entity)
    {
        $encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword());
        $entity->setPassword($encoded);
    }
}