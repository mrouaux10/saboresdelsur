<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 21/09/17
 * Time: 17:50
 */

namespace AppBundle\Controller;


use AppBundle\Form\loginForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     */
    public function Action()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(loginForm::class, [
            '_username' => $lastUsername
            ]);

        return $this->render('security/login.html.twig', array(
            'formLogin' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function LogoutAction()
    {
        throw  new \Exception('Esto no debe ser alcanzado');
    }

}