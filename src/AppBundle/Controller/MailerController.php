<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 26/09/17
 * Time: 23:51
 */

namespace AppBundle\Controller;


use AppBundle\Form\orderForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MailerController extends Controller
{
    /**
     * @Route("/user/{nombre}-{email}-{telefono}-{codigoPedido}-{descripcion}-{fechaEsperada}-{categoria}", name="user_order", requirements={"fechaEsperada"=".+","codigoPedido"=".+"})
     */
    public function MailAction($nombre, $email, $telefono, $codigoPedido, $descripcion, $fechaEsperada, $categoria, \Swift_Mailer $mailer)
    {
        #  Este mensaje se entregara a las reposteras para que contacten al interesado.
        $message = (new \Swift_Message('Nueva consulta recibida!'))
            ->setFrom($email)
            ->setTo('saboresdelsur.pedidos@gmail.com')
            ->setCc('mrouaux10@gmail.com')
            ->setBody(
                $this->renderView(
                    'email/order.html.twig',array(
                        'nombre' => $nombre,
                        'email' => $email,
                        'telefono' => $telefono,
                        'codigoPedido' => $codigoPedido,
                        'descripcion' => $descripcion,
                        'fechaEsperada' =>$fechaEsperada,
                        'categoria' => $categoria
                    )
                )
            )
        ;
        #  Este mensaje se entregara al interesado, a modo de agradecimiento.
        $message2 = (new \Swift_Message('Gracias por elegirnos!'))
            ->setFrom('saboresdelsur.pedidos@gmail.com')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'email/personaInteresada.html.twig',array(
                        'nombre' => $nombre
                    )
                )
            )
        ;

        $mailer->send($message);
        $mailer->send($message2);

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/user/{nombre}-{email}-{tema}-{mensaje}", name="user_contact")
     */
    public function ContactAction($nombre, $email, $tema, $mensaje, \Swift_Mailer $mailer)
    {
        #  Este mensaje se entregara a las reposteras para sepan que fueron contactadas.
        $message = (new \Swift_Message('Alguien se contactó contigo!'))
            ->setFrom($email)
            ->setTo('saboresdelsur.pedidos@gmail.com')
            ->setCc('mrouaux10@gmail.com')
            ->setBody(
                $this->renderView(
                    'email/contact.html.twig',array(
                        'nombre' => $nombre,
                        'email' => $email,
                        'tema' => $tema,
                        'mensaje' => $mensaje,
                    )
                )
            )
        ;

        #  Este mensaje se entregara al interesado, a modo de agradecimiento por contactarse.
        $message2 = (new \Swift_Message('Gracias por contactarnos!'))
            ->setFrom('saboresdelsur.pedidos@gmail.com')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'email/personaInteresadaContacto.html.twig',array(
                        'nombre' => $nombre
                    )
                )
            )
        ;

        $mailer->send($message);
        $mailer->send($message2);
        return $this->redirectToRoute('homepage');
    }


    public function otherAction($order)
    {
        // some other logic
        // in this case $entity equals 'some_value'

        $real_entity = $this->get('some_service')->get($order);
        dump($real_entity);die;

    }

}