<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Producto;
use AppBundle\Form\contactForm;
use AppBundle\Form\orderForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $tortas = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->obtenerTortas();
        $tartas = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->obtenerTartas();
        $mesasDulces = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->obtenerMesasDulces();
        $variedades = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->obtenerVariedades();
        $formOrder = $this->createForm(orderForm::class);

        $formOrder->handleRequest($request);

        if ($formOrder->isSubmitted() && $formOrder->isValid()){
            $nombre = $formOrder["name"]->getData();
            $email = $formOrder["email"]->getData();
            $telefono = $formOrder["phone"]->getData();
            $codigoPedido = $formOrder["orderCode"]->getData();
            $descripcion = $formOrder["descripcion"]->getData();
            $fecha = $formOrder["dateExpected"]->getData();
            $fechaEsperada= $fecha->format(('d/m/Y'));
            $elemento = $this->getDoctrine()
                ->getRepository(Producto::class)
                ->findOneBy(['codigo' => $codigoPedido]);
            $categoriaCompleto = $elemento->getCodigo();
            # (Substraigo las primeras dos letras identificatorias de categoria)
            $categoria = substr($categoriaCompleto,0,2);
            switch ($categoria){
                case 'TO':
                    $categoria = 'Torta';
                    break;
                case 'TA':
                    $categoria = 'Tarta';
                    break;
                case 'MD':
                    $categoria = 'Mesa dulce';
                    break;
                case 'VA':
                    $categoria = 'Variedad';
                    break;
                default:
                    throw new Exception('Categoria no identificada');
                    break;
            }

            $this->get('session')->getFlashBag()->add('mensajeOrdenPedidoExito', 'Excelente! Tu consulta fue enviada con exito, te contactaremos a la brevedad :)');

            return $this->redirectToRoute('user_order',array(
                'nombre' => $nombre,
                'email' => $email,
                'telefono' => $telefono,
                'codigoPedido' => $codigoPedido,
                'descripcion' => $descripcion,
                'fechaEsperada' => $fechaEsperada,
                'categoria' => $categoria,
            ));
        }

        $formContact = $this->createForm(contactForm::class);
        $formContact->handleRequest($request);
        if ($formContact->isSubmitted() && $formContact->isValid()){
            $nombre = $formContact["name"]->getData();
            $email = $formContact["email"]->getData();
            $tema = $formContact["topic"]->getData();
            $mensaje = $formContact["mensaje"]->getData();

            $this->get('session')->getFlashBag()->add('mensajeContactoExito', 'Excelente! Tu mensaje fue enviado con exito, te contactaremos a la brevedad :)');

            return $this->redirectToRoute('user_contact', array(
               'nombre' => $nombre,
               'email' => $email,
               'tema' => $tema,
               'mensaje' => $mensaje,
            ));


        }


        // replace this example code with whatever you need
        return $this->render('index/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'tortas' => $tortas,
            'tartas' => $tartas,
            'mesasDulces' => $mesasDulces,
            'variedades' => $variedades,
            'formOrder' => $formOrder->createView(),
            'formContact' => $formContact->createView(),
        ]);





    }
}
