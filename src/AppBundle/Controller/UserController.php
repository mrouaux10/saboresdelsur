<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 28/09/17
 * Time: 19:34
 */

namespace AppBundle\Controller;


use AppBundle\Form\userForm;
use AppBundle\Security\LoginFormAuthenticator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/user", name="user_register")
     */
    public function RegisterAction(Request $request, LoginFormAuthenticator $authenticator){
        $form = $this->createForm(userForm::class);


        $form->handleRequest($request);


        if ($form->isValid()){
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Bienvenido '.$user->getEmail().'!');

            return $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $authenticator,
                    'main'
                );
        }


        return $this->render('user/register.html.twig', [
            'registerForm' => $form->createView()
        ]);
    }
}