<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 23/09/17
 * Time: 17:09
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Producto;
use AppBundle\Entity\Torta;
use AppBundle\Form\contactForm;
use AppBundle\Form\productForm;
use AppBundle\Form\loginForm;
use AppBundle\Form\uploadFileForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @Route("/profile", name="admin_profile")
     */
    public function ProfileAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $productos = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->findAll();

        return $this->render('admin/profile.html.twig', [
            'productos' => $productos,
        ]);
    }

    /**
     * @Route("/profile/new", name="product_new")
     */
    public function newAction(\Symfony\Component\HttpFoundation\Request $request)
    {

        $form = $this->createForm(uploadFileForm::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $imagen = $form["imageFile"]->getData();
            $elementoASubir = $form["categoria"]->getData();
            $tituloDescripcion = $form["tituloDescripcion"]->getData();
            $descripcion = $form["descripcion"]->getData();
            $producto = new Producto();
            $producto->setImageFile($imagen);
            $mensaje = 'exito';
            $tipoDeDato = $producto->getImageFile()->getMimeType();
            $tamañoMaximo = $producto->getImageFile()->getSize();

            if ($tipoDeDato == 'image/jpeg' || $tipoDeDato == 'image/jpg' || $tipoDeDato == 'image/png'){
                if ($tamañoMaximo <= 1500000){

                    if ($tituloDescripcion == null){
                        $producto->setTituloDescripcion('Sin título');
                    }else{
                        $producto->setTituloDescripcion($tituloDescripcion);
                    }
                    if ($descripcion == null){
                        $producto->setDescripcion('Sin descripción');
                    }else{
                        $producto->setDescripcion($descripcion);
                    }
                    $date = new \DateTime();
                    $codigo = $date->format(('YmdHis'));

                    switch ($elementoASubir){
                        case 'Torta':
                            $producto->setCodigo('TO-'.$codigo);
                            $producto->setCategoria('Torta');
                            break;
                        case 'Tarta':
                            $producto->setCodigo('TA-'.$codigo);
                            $producto->setCategoria('Tarta');
                            break;
                        case 'Mesa dulce':
                            $producto->setCodigo('MD-'.$codigo);
                            $producto->setCategoria('Mesa dulce');
                            break;
                        case 'Variedad':
                            $producto->setCodigo('VA-'.$codigo);
                            $producto->setCategoria('Variedad');
                            break;
                    }
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($producto);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add('mensajeExito', 'Bien hecho! La imagen fue cargada exitosamente :)');
                    return $this->redirectToRoute('homepage');
                }
                else{
                    $this->get('session')->getFlashBag()->add('mensajeLimiteTamaño', 'Ohh! Parece que la imagen es muy grande, prueba con una mas chica :)');
                    return $this->redirectToRoute('homepage');
                }
            }
            else{
                $this->get('session')->getFlashBag()->add('mensajeTipoArchivo', 'Ohh! No se pudo reconocer la imagen, prueba con uno de los siguientes formatos: png, jpg, jpeg :)');
                return $this->redirectToRoute('homepage');
            }

        }

        return $this->render('admin/newProduct.html.twig', [
            'uploadFileForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile/{codigo}/edit", name="product_edit")
     */
    public function ProductEditAction(Request $request, Producto $prod)
    {

        $form = $this->createForm(uploadFileForm::class, $prod);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $nombreImagenInicial = $prod->getImage()->getOriginalName();
            $tipoDeDato = $prod->getImage()->getMimeType();
            $tamaño = $prod->getImage()->getSize();
            $elementoASubir = $form["categoria"]->getData();
            $tituloDescripcion = $form["tituloDescripcion"]->getData();
            $descripcion = $form["descripcion"]->getData();
            $mensaje = 'exito';

            if ($tipoDeDato == 'image/jpeg' || $tipoDeDato == 'image/jpg' || $tipoDeDato == 'image/png'){
                if ($tamaño <= 800000){

                    if ($tituloDescripcion == null){
                        $prod->setTituloDescripcion('Sin título');
                    }else{
                        $prod->setTituloDescripcion($tituloDescripcion);
                    }
                    if ($descripcion == null){
                        $prod->setDescripcion('Sin descripción');
                    }else{
                        $prod->setDescripcion($descripcion);
                    }
                    $date = new \DateTime();
                    $codigo = $date->format(('YmdHis'));
                    switch ($elementoASubir){
                        case 'Torta':
                            $prod->setCodigo('TO-'.$codigo);
                            $prod->setCategoria('Torta');
                            break;
                        case 'Tarta':
                            $prod->setCodigo('TA-'.$codigo);
                            $prod->setCategoria('Tarta');
                            break;
                        case 'Mesa dulce':
                            $prod->setCodigo('MD-'.$codigo);
                            $prod->setCategoria('Mesa dulce');
                            break;
                        case 'Variedad':
                            $prod->setCodigo('VA-'.$codigo);
                            $prod->setCategoria('Variedad');
                            break;
                    }

/*                    # Al editar una img, le agrego un codigo al
                    # originalName de la misma para evitar problemas de duplicado por igual nombre de img
                    # al querer eliminar alguna en un futuro:

                    $nombreImagenFinal = $codigo.$nombreImagenInicial;
                    $prod->getImage()->setName($nombreImagenFinal);
                    $prod->getImage()->setOriginalName($nombreImagenFinal);*/

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($prod);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add('mensajeExito', 'Bien hecho! La imagen fue cargada exitosamente :)');
                    return $this->redirectToRoute('homepage');
                }
                else{
                    $this->get('session')->getFlashBag()->add('mensajeLimiteTamaño', 'Ohh! Parece que la imagen es muy grande, prueba con una mas chica :)');
                    return $this->redirectToRoute('homepage');
                }
            }
            else{
                $this->get('session')->getFlashBag()->add('mensajeTipoArchivo', 'Ohh! No se pudo reconocer la imagen, prueba con uno de los siguientes formatos: png, jpg, jpeg :)');
                return $this->redirectToRoute('homepage');
            }

        }



        return $this->render('admin/editImage.html.twig', [
            'uploadFileForm' => $form->createView(),
            'producto' => $prod,
        ]);
    }

    /**
     * @Route("/profile/{codigo}/remove", name="product_remove")
     */
    public function ProductDeleteAction($codigo){

        $producto = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->createQueryBuilder('prod')
            ->delete('AppBundle:Producto','prod')
            ->andWhere('prod.codigo = :cod')
            ->setParameter('cod', $codigo)
            ->getQuery()
            ->execute();



#        $fileSystem = new Filesystem();
#        $fileSystem->remove($producto->getCodigo());
#        sleep(3);
        $this->get('session')->getFlashBag()->add('mensajeBorradoArchivo', 'Bien hecho! La imagen fue eliminada exitosamente :)');
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/profile/{codigo}/search", name="product_search")
     */
    public function ProductSearchAction($codigo){

        return $this->redirectToRoute('product_edit', array(
            'codigo' => $codigo
        ));
    }
}
