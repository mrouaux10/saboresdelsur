<?php

/* :admin:profile.html.twig */
class __TwigTemplate_ed94b3943ec3ad5d049add479bcbf6127bc72edcb1fd86bb652bb00e7408f021 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":admin:profile.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'banner' => array($this, 'block_banner'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0754f4743535dd9a720f08f45a3b21ce81c4ab9b5a916617356f1631e801b54c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0754f4743535dd9a720f08f45a3b21ce81c4ab9b5a916617356f1631e801b54c->enter($__internal_0754f4743535dd9a720f08f45a3b21ce81c4ab9b5a916617356f1631e801b54c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":admin:profile.html.twig"));

        $__internal_460715de4215e8c6121ac640039c7ffe7dda11dac38f464a6c926c13d2e4fba7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_460715de4215e8c6121ac640039c7ffe7dda11dac38f464a6c926c13d2e4fba7->enter($__internal_460715de4215e8c6121ac640039c7ffe7dda11dac38f464a6c926c13d2e4fba7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":admin:profile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0754f4743535dd9a720f08f45a3b21ce81c4ab9b5a916617356f1631e801b54c->leave($__internal_0754f4743535dd9a720f08f45a3b21ce81c4ab9b5a916617356f1631e801b54c_prof);

        
        $__internal_460715de4215e8c6121ac640039c7ffe7dda11dac38f464a6c926c13d2e4fba7->leave($__internal_460715de4215e8c6121ac640039c7ffe7dda11dac38f464a6c926c13d2e4fba7_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_302a2461b4496bf11e24beca437c8b94cbc6aa7506070d4ca8cd83a1bdf135ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_302a2461b4496bf11e24beca437c8b94cbc6aa7506070d4ca8cd83a1bdf135ca->enter($__internal_302a2461b4496bf11e24beca437c8b94cbc6aa7506070d4ca8cd83a1bdf135ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_2275162d7224ed62a6f3376bc86dd5203efc992925d40d4504907ff3ff8369d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2275162d7224ed62a6f3376bc86dd5203efc992925d40d4504907ff3ff8369d9->enter($__internal_2275162d7224ed62a6f3376bc86dd5203efc992925d40d4504907ff3ff8369d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sabores del Sur";
        
        $__internal_2275162d7224ed62a6f3376bc86dd5203efc992925d40d4504907ff3ff8369d9->leave($__internal_2275162d7224ed62a6f3376bc86dd5203efc992925d40d4504907ff3ff8369d9_prof);

        
        $__internal_302a2461b4496bf11e24beca437c8b94cbc6aa7506070d4ca8cd83a1bdf135ca->leave($__internal_302a2461b4496bf11e24beca437c8b94cbc6aa7506070d4ca8cd83a1bdf135ca_prof);

    }

    // line 6
    public function block_banner($context, array $blocks = array())
    {
        $__internal_b293aba4ee1fa566c4dd5e0bb7469bb02217a3c783246775bfd3bee16dde379e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b293aba4ee1fa566c4dd5e0bb7469bb02217a3c783246775bfd3bee16dde379e->enter($__internal_b293aba4ee1fa566c4dd5e0bb7469bb02217a3c783246775bfd3bee16dde379e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        $__internal_bfbe691e9d057b11a73412638cab33a8a3996d5ee997f1600a7b2669c4521704 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfbe691e9d057b11a73412638cab33a8a3996d5ee997f1600a7b2669c4521704->enter($__internal_bfbe691e9d057b11a73412638cab33a8a3996d5ee997f1600a7b2669c4521704_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        // line 7
        echo "            <div>
                <a class=\"animated bounceInLeft navbar-brand\" href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Sabores del Sur</a>
                <div class=\"container\">
                    <div class=\"col-lg-offset-6 col-md-offset-5 col-sm-offset-3\">
                <ul class=\"hidden-xs nav navbar-nav main-nav clear\">
                    <li><a class=\"navactive color_animation\">";
        // line 12
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->getSourceContext()); })()), "user", array())) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->getSourceContext()); })()), "user", array()), "nombre", array())) : ("")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->getSourceContext()); })()), "user", array())) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->getSourceContext()); })()), "user", array()), "apellido", array())) : ("")), "html", null, true);
        echo "</a></li>
                    <!--Pregunto si esta logueado o no para iniciar o cerrar sesion-->
                    ";
        // line 14
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 15
            echo "                        <li><a></a></li>
                        <li><a class=\"color_animation\" href=\"";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
            echo "\">Volver a Inicio</a></li>
                        <li><a class=\"color_animation\" href=\"";
            // line 17
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">Cerrar Sesión</a></li>
                    ";
        }
        // line 19
        echo "                </ul>
                </div>
              </div>
            </div>
        ";
        
        $__internal_bfbe691e9d057b11a73412638cab33a8a3996d5ee997f1600a7b2669c4521704->leave($__internal_bfbe691e9d057b11a73412638cab33a8a3996d5ee997f1600a7b2669c4521704_prof);

        
        $__internal_b293aba4ee1fa566c4dd5e0bb7469bb02217a3c783246775bfd3bee16dde379e->leave($__internal_b293aba4ee1fa566c4dd5e0bb7469bb02217a3c783246775bfd3bee16dde379e_prof);

    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        $__internal_900d5120c4f31bfb58adc422e0e5fd5672e212d74ff303484219aa666968a222 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_900d5120c4f31bfb58adc422e0e5fd5672e212d74ff303484219aa666968a222->enter($__internal_900d5120c4f31bfb58adc422e0e5fd5672e212d74ff303484219aa666968a222_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d926799a163cdb7106d168f4a847b48dfce563ecdc9af52b7bb6ebaa177606f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d926799a163cdb7106d168f4a847b48dfce563ecdc9af52b7bb6ebaa177606f8->enter($__internal_d926799a163cdb7106d168f4a847b48dfce563ecdc9af52b7bb6ebaa177606f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "    <div id=\"top\" class=\"starter_container_profile bg\">
            <div class=\"follow_container\">
                <div class=\"col-md-6 col-md-offset-3\">
                    <h2 class=\"animated tada\">Bienvenida ";
        // line 28
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 28, $this->getSourceContext()); })()), "user", array())) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 28, $this->getSourceContext()); })()), "user", array()), "nombre", array())) : ("")), "html", null, true);
        echo " !!</h2>
                    <br>
                    <hr>
                    <h3 class=\"animated rubberBand\">Aquí podrás administrar tu galeria de fotos</h3>
                </div>
            </div>
        </div>

        <!-- ============ Galeria de fotos ============= -->
    <section>
        <br>
        <div class=\"container\">
            <div class=\"row\">
                <hr>
                    <div class=\"col-md-12\">
                        <h2 class=\"title\"><em>Mira como luce tu galeria...</em></h2>
                    </div>
                <hr>
                <div class=\"col-md-12\">
                    <p style=\"text-align: center\" class=\"desc-text\"><em>Seleccione un elemento y precione en \"Editar\" para modificarlo o eliminarlo.</em></p><br>
                </div>
                <div class=\"col-md-12\">
                    <div align=\"center\">
                        <button class=\"btn btn-success filter-button\" data-filter=\"all\">Todos</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"hdpe\">Tortas</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"sprinkle\">Tartas</button>
                        <br class=\"hidden-lg hidden-md\">
                        <br class=\"hidden-lg hidden-md\">
                        <button class=\"btn btn-info filter-button\" data-filter=\"spray\">Mesas Dulces</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"irrigation\">Variedades</button>
                    </div>
                </div>
            </div>
                <br>

            <div class=\"row\">
                <div class=\"hidden-xs col-lg-3 col-md-4 col-sm-5\">
                    <a  class=\"btn btn-success scrollflow -slide-right -opacity\" href=\"";
        // line 65
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_register");
        echo "\">Nuevo Usuario</a>
                    <a  class=\"btn btn-success scrollflow -slide-right -opacity\" href=\"";
        // line 66
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_new");
        echo "\">Nuevo Producto</a>
                </div>
                <div style=\"text-align: center\" class=\"hidden-lg hidden-md hidden-sm col-xs-12\">
                    <a  class=\"btn btn-success btn-xs scrollflow -slide-right -opacity\" href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_register");
        echo "\">Nuevo Usuario</a>
                    <a  class=\"btn btn-success btn-xs scrollflow -slide-right -opacity\" href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_new");
        echo "\">Nuevo Producto</a>
                    <br><br>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-10 col-xs-offset-1\">
                    <form action=\"javascript:getCodigo()\" class=\"search-form\">
                        <div class=\"form-group has-feedback\">
                            <input type=\"text\" class=\"form-control\" name=\"search\" id=\"search\" placeholder=\"Buscar producto por código\">
                            <span onclick=\"getCodigo()\" class=\"glyphicon glyphicon-search form-control-feedback\"></span>
                        </div>
                    </form>
                </div>
            </div>

                <br>
            <div class=\"row\">
                ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) || array_key_exists("productos", $context) ? $context["productos"] : (function () { throw new Twig_Error_Runtime('Variable "productos" does not exist.', 85, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
            // line 86
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "categoria", array()) == "Torta")) {
                // line 87
                echo "                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter hdpe\">
                            <a href=\"";
                // line 88
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"";
                // line 89
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_remove", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
                // line 91
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "getImage", array()), "getOriginalName", array()), "html", null, true);
                echo "' alt=\"Card image cap\">
                        </div>
                    ";
            }
            // line 94
            echo "
                    ";
            // line 95
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "categoria", array()) == "Tarta")) {
                // line 96
                echo "                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter sprinkle\">
                            <a href=\"";
                // line 97
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"";
                // line 98
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_remove", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
                // line 100
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "getImage", array()), "getOriginalName", array()), "html", null, true);
                echo "' alt=\"Card image cap\">
                        </div>
                    ";
            }
            // line 103
            echo "
                    ";
            // line 104
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "categoria", array()) == "Mesa dulce")) {
                // line 105
                echo "                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter spray\">
                            <a href=\"";
                // line 106
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a data-toggle=\"modal\" data-target=\"#myModal2\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "getImage", array()), "getOriginalName", array()), "html", null, true);
                echo "' alt=\"Card image cap\">
                        </div>
                    ";
            }
            // line 112
            echo "
                    ";
            // line 113
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "categoria", array()) == "Variedad")) {
                // line 114
                echo "                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter irrigation\">
                            <a href=\"";
                // line 115
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_remove", array("codigo" => twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "codigo", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
                // line 118
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["producto"], "getImage", array()), "getOriginalName", array()), "html", null, true);
                echo "' alt=\"Card image cap\">
                        </div>
                    ";
            }
            // line 121
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "
            </div>

            </div>
        </section>
        <!-- Modal -->
        <div class=\"modal fade\" id=\"myModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModal2Title\" aria-hidden=\"true\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>
                    </div>
                    <div class=\"modal-body\">
                        <em><p style=\"font-size: 15px;\" id=\"textoDescripcion\"></p>
                            <p style=\"font-size: 15px;\" class=\"desc-text\">Seguro deseas eliminar esta foto?</p>
                        </em>
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                        <button href=\"";
        // line 143
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => "x"));
        echo "\" type=\"button\" id=\"botonModal\" class=\"btn btn-primary\">Si, eliminar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============ Footer Section  ============= -->

    <footer class=\"sub_footer\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-5\">
                    <p class=\"sub-footer-text text-center\">&copy; Sabores del Sur 2017</p>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">
                    <p class=\"sub-footer-text text-center\">Volver al <a href=\"#top\">Inicio</a></p>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-3\">
                    ";
        // line 161
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 162
            echo "                        <a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">Cerrar Sesión</a><
                    ";
        } else {
            // line 164
            echo "                        <a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
            echo "\">Iniciar Sesión</a>
                    ";
        }
        // line 166
        echo "                </div>
            </div>
        </div>
    </footer>
    ";
        
        $__internal_d926799a163cdb7106d168f4a847b48dfce563ecdc9af52b7bb6ebaa177606f8->leave($__internal_d926799a163cdb7106d168f4a847b48dfce563ecdc9af52b7bb6ebaa177606f8_prof);

        
        $__internal_900d5120c4f31bfb58adc422e0e5fd5672e212d74ff303484219aa666968a222->leave($__internal_900d5120c4f31bfb58adc422e0e5fd5672e212d74ff303484219aa666968a222_prof);

    }

    // line 172
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4ad9758e28a5bc42b9ced0ffb0d60eee8262f5575c013680e6f0e4403729fc8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ad9758e28a5bc42b9ced0ffb0d60eee8262f5575c013680e6f0e4403729fc8b->enter($__internal_4ad9758e28a5bc42b9ced0ffb0d60eee8262f5575c013680e6f0e4403729fc8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_ca128321c02c94d32709eb6b4a8cad9f22e53b1ee7b4309071444fd1cb175461 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca128321c02c94d32709eb6b4a8cad9f22e53b1ee7b4309071444fd1cb175461->enter($__internal_ca128321c02c94d32709eb6b4a8cad9f22e53b1ee7b4309071444fd1cb175461_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 173
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script>
            function getCodigo() {
                var cod = document.getElementById('search').value;
                url = \"";
        // line 177
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("codigo" => "CODIGO"));
        echo "\";
                url = url.replace(\"CODIGO\", cod);
                    location.href = url;
            }
        </script>
    ";
        
        $__internal_ca128321c02c94d32709eb6b4a8cad9f22e53b1ee7b4309071444fd1cb175461->leave($__internal_ca128321c02c94d32709eb6b4a8cad9f22e53b1ee7b4309071444fd1cb175461_prof);

        
        $__internal_4ad9758e28a5bc42b9ced0ffb0d60eee8262f5575c013680e6f0e4403729fc8b->leave($__internal_4ad9758e28a5bc42b9ced0ffb0d60eee8262f5575c013680e6f0e4403729fc8b_prof);

    }

    public function getTemplateName()
    {
        return ":admin:profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 177,  379 => 173,  370 => 172,  356 => 166,  350 => 164,  344 => 162,  342 => 161,  321 => 143,  298 => 122,  292 => 121,  286 => 118,  281 => 116,  277 => 115,  274 => 114,  272 => 113,  269 => 112,  263 => 109,  257 => 106,  254 => 105,  252 => 104,  249 => 103,  243 => 100,  238 => 98,  234 => 97,  231 => 96,  229 => 95,  226 => 94,  220 => 91,  215 => 89,  211 => 88,  208 => 87,  205 => 86,  201 => 85,  183 => 70,  179 => 69,  173 => 66,  169 => 65,  129 => 28,  124 => 25,  115 => 24,  101 => 19,  96 => 17,  92 => 16,  89 => 15,  87 => 14,  80 => 12,  73 => 8,  70 => 7,  61 => 6,  43 => 4,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


    {% block title %}Sabores del Sur{% endblock %}

        {% block banner %}
            <div>
                <a class=\"animated bounceInLeft navbar-brand\" href=\"{{ path('homepage') }}\">Sabores del Sur</a>
                <div class=\"container\">
                    <div class=\"col-lg-offset-6 col-md-offset-5 col-sm-offset-3\">
                <ul class=\"hidden-xs nav navbar-nav main-nav clear\">
                    <li><a class=\"navactive color_animation\">{{ app.user ? app.user.nombre: '' }} {{ app.user ? app.user.apellido: '' }}</a></li>
                    <!--Pregunto si esta logueado o no para iniciar o cerrar sesion-->
                    {% if is_granted('ROLE_USER') %}
                        <li><a></a></li>
                        <li><a class=\"color_animation\" href=\"{{ path('homepage') }}\">Volver a Inicio</a></li>
                        <li><a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a></li>
                    {% endif %}
                </ul>
                </div>
              </div>
            </div>
        {% endblock %}
{% block body %}
    <div id=\"top\" class=\"starter_container_profile bg\">
            <div class=\"follow_container\">
                <div class=\"col-md-6 col-md-offset-3\">
                    <h2 class=\"animated tada\">Bienvenida {{ app.user ? app.user.nombre }} !!</h2>
                    <br>
                    <hr>
                    <h3 class=\"animated rubberBand\">Aquí podrás administrar tu galeria de fotos</h3>
                </div>
            </div>
        </div>

        <!-- ============ Galeria de fotos ============= -->
    <section>
        <br>
        <div class=\"container\">
            <div class=\"row\">
                <hr>
                    <div class=\"col-md-12\">
                        <h2 class=\"title\"><em>Mira como luce tu galeria...</em></h2>
                    </div>
                <hr>
                <div class=\"col-md-12\">
                    <p style=\"text-align: center\" class=\"desc-text\"><em>Seleccione un elemento y precione en \"Editar\" para modificarlo o eliminarlo.</em></p><br>
                </div>
                <div class=\"col-md-12\">
                    <div align=\"center\">
                        <button class=\"btn btn-success filter-button\" data-filter=\"all\">Todos</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"hdpe\">Tortas</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"sprinkle\">Tartas</button>
                        <br class=\"hidden-lg hidden-md\">
                        <br class=\"hidden-lg hidden-md\">
                        <button class=\"btn btn-info filter-button\" data-filter=\"spray\">Mesas Dulces</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"irrigation\">Variedades</button>
                    </div>
                </div>
            </div>
                <br>

            <div class=\"row\">
                <div class=\"hidden-xs col-lg-3 col-md-4 col-sm-5\">
                    <a  class=\"btn btn-success scrollflow -slide-right -opacity\" href=\"{{ path('user_register') }}\">Nuevo Usuario</a>
                    <a  class=\"btn btn-success scrollflow -slide-right -opacity\" href=\"{{ path('product_new') }}\">Nuevo Producto</a>
                </div>
                <div style=\"text-align: center\" class=\"hidden-lg hidden-md hidden-sm col-xs-12\">
                    <a  class=\"btn btn-success btn-xs scrollflow -slide-right -opacity\" href=\"{{ path('user_register') }}\">Nuevo Usuario</a>
                    <a  class=\"btn btn-success btn-xs scrollflow -slide-right -opacity\" href=\"{{ path('product_new') }}\">Nuevo Producto</a>
                    <br><br>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-10 col-xs-offset-1\">
                    <form action=\"javascript:getCodigo()\" class=\"search-form\">
                        <div class=\"form-group has-feedback\">
                            <input type=\"text\" class=\"form-control\" name=\"search\" id=\"search\" placeholder=\"Buscar producto por código\">
                            <span onclick=\"getCodigo()\" class=\"glyphicon glyphicon-search form-control-feedback\"></span>
                        </div>
                    </form>
                </div>
            </div>

                <br>
            <div class=\"row\">
                {% for producto in productos%}
                    {% if producto.categoria == 'Torta' %}
                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter hdpe\">
                            <a href=\"{{ path('product_edit', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"{{ path('product_remove', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ producto.getImage.getOriginalName }}' alt=\"Card image cap\">
                        </div>
                    {% endif %}

                    {% if producto.categoria == 'Tarta' %}
                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter sprinkle\">
                            <a href=\"{{ path('product_edit', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"{{ path('product_remove', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ producto.getImage.getOriginalName }}' alt=\"Card image cap\">
                        </div>
                    {% endif %}

                    {% if producto.categoria == 'Mesa dulce' %}
                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter spray\">
                            <a href=\"{{ path('product_edit', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a data-toggle=\"modal\" data-target=\"#myModal2\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ producto.getImage.getOriginalName }}' alt=\"Card image cap\">
                        </div>
                    {% endif %}

                    {% if producto.categoria == 'Variedad' %}
                        <div align=\"middle\" class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter irrigation\">
                            <a href=\"{{ path('product_edit', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-success\"><span style=\"width: 20px;\" class=\"fa fa-pencil\"></span></a>
                            <a href=\"{{ path('product_remove', { 'codigo': producto.codigo}) }}\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-trash\"></span></a>
                            <br><br>
                            <img style=\"width: 320px; height: 180px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ producto.getImage.getOriginalName }}' alt=\"Card image cap\">
                        </div>
                    {% endif %}
                {% endfor %}

            </div>

            </div>
        </section>
        <!-- Modal -->
        <div class=\"modal fade\" id=\"myModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModal2Title\" aria-hidden=\"true\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>
                    </div>
                    <div class=\"modal-body\">
                        <em><p style=\"font-size: 15px;\" id=\"textoDescripcion\"></p>
                            <p style=\"font-size: 15px;\" class=\"desc-text\">Seguro deseas eliminar esta foto?</p>
                        </em>
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancelar</button>
                        <button href=\"{{ path('product_edit', {'codigo': 'x'}) }}\" type=\"button\" id=\"botonModal\" class=\"btn btn-primary\">Si, eliminar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============ Footer Section  ============= -->

    <footer class=\"sub_footer\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-5\">
                    <p class=\"sub-footer-text text-center\">&copy; Sabores del Sur 2017</p>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">
                    <p class=\"sub-footer-text text-center\">Volver al <a href=\"#top\">Inicio</a></p>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-3\">
                    {% if is_granted('ROLE_USER') %}
                        <a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a><
                    {% else %}
                        <a class=\"color_animation\" href=\"{{ path('security_login') }}\">Iniciar Sesión</a>
                    {% endif %}
                </div>
            </div>
        </div>
    </footer>
    {% endblock %}

    {% block javascripts %}
        {{ parent() }}
        <script>
            function getCodigo() {
                var cod = document.getElementById('search').value;
                url = \"{{ path('product_edit', {'codigo': 'CODIGO'}) }}\";
                url = url.replace(\"CODIGO\", cod);
                    location.href = url;
            }
        </script>
    {% endblock %}

", ":admin:profile.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/app/Resources/views/admin/profile.html.twig");
    }
}
