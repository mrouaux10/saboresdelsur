<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_83d5e0d412c274cd9f9a9e19d4d153394aa454eaeb61d53eee7ddf4328c72713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dcf3e25a4991818b1b4295f2765d341c21b3f634cd635b2b58c70708e08ec7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dcf3e25a4991818b1b4295f2765d341c21b3f634cd635b2b58c70708e08ec7a->enter($__internal_0dcf3e25a4991818b1b4295f2765d341c21b3f634cd635b2b58c70708e08ec7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_d9e857d621931e84a37aa79ae689ce0f68d912efab27db254ba5a70a3c4e70f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9e857d621931e84a37aa79ae689ce0f68d912efab27db254ba5a70a3c4e70f2->enter($__internal_d9e857d621931e84a37aa79ae689ce0f68d912efab27db254ba5a70a3c4e70f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_0dcf3e25a4991818b1b4295f2765d341c21b3f634cd635b2b58c70708e08ec7a->leave($__internal_0dcf3e25a4991818b1b4295f2765d341c21b3f634cd635b2b58c70708e08ec7a_prof);

        
        $__internal_d9e857d621931e84a37aa79ae689ce0f68d912efab27db254ba5a70a3c4e70f2->leave($__internal_d9e857d621931e84a37aa79ae689ce0f68d912efab27db254ba5a70a3c4e70f2_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
