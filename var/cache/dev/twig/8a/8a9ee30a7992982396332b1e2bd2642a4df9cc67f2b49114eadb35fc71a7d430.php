<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_5519c334d63c29e87cb5f773ea8487d78d52c6c7bed1c08974fbc2fb1fbd6dda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcd31de15ef1cfc669f85ec87d1dccdcf4b5675ccd4739d9e1b257ecbe7ad8db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcd31de15ef1cfc669f85ec87d1dccdcf4b5675ccd4739d9e1b257ecbe7ad8db->enter($__internal_dcd31de15ef1cfc669f85ec87d1dccdcf4b5675ccd4739d9e1b257ecbe7ad8db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_59dc3adf8e02e26d6cb0a87cc99a078225aa05c895c1d942e86af778cb7c0412 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59dc3adf8e02e26d6cb0a87cc99a078225aa05c895c1d942e86af778cb7c0412->enter($__internal_59dc3adf8e02e26d6cb0a87cc99a078225aa05c895c1d942e86af778cb7c0412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dcd31de15ef1cfc669f85ec87d1dccdcf4b5675ccd4739d9e1b257ecbe7ad8db->leave($__internal_dcd31de15ef1cfc669f85ec87d1dccdcf4b5675ccd4739d9e1b257ecbe7ad8db_prof);

        
        $__internal_59dc3adf8e02e26d6cb0a87cc99a078225aa05c895c1d942e86af778cb7c0412->leave($__internal_59dc3adf8e02e26d6cb0a87cc99a078225aa05c895c1d942e86af778cb7c0412_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dfbb7dcf550118bde000ac50b9963d48916dcb07026294ef6c2c021a08564863 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfbb7dcf550118bde000ac50b9963d48916dcb07026294ef6c2c021a08564863->enter($__internal_dfbb7dcf550118bde000ac50b9963d48916dcb07026294ef6c2c021a08564863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_e554cb603a2e52a8d636784469ebaf089ead1f95853ac306092c11ea7f579f3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e554cb603a2e52a8d636784469ebaf089ead1f95853ac306092c11ea7f579f3d->enter($__internal_e554cb603a2e52a8d636784469ebaf089ead1f95853ac306092c11ea7f579f3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_e554cb603a2e52a8d636784469ebaf089ead1f95853ac306092c11ea7f579f3d->leave($__internal_e554cb603a2e52a8d636784469ebaf089ead1f95853ac306092c11ea7f579f3d_prof);

        
        $__internal_dfbb7dcf550118bde000ac50b9963d48916dcb07026294ef6c2c021a08564863->leave($__internal_dfbb7dcf550118bde000ac50b9963d48916dcb07026294ef6c2c021a08564863_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
