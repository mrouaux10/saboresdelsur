<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_13725bb4984dacb889e28b5ae1d80e21d291a56197fed532c2570d66cd0c2f49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_685000e8c9dba6fec59e848111e812538c5aa7ee5f56247fc9b3ef310c8f2215 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_685000e8c9dba6fec59e848111e812538c5aa7ee5f56247fc9b3ef310c8f2215->enter($__internal_685000e8c9dba6fec59e848111e812538c5aa7ee5f56247fc9b3ef310c8f2215_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_75f4ef706bacae942d03fbaa3fa6109dfd48b1b96fab488affb48e161dc7b4ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75f4ef706bacae942d03fbaa3fa6109dfd48b1b96fab488affb48e161dc7b4ff->enter($__internal_75f4ef706bacae942d03fbaa3fa6109dfd48b1b96fab488affb48e161dc7b4ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_685000e8c9dba6fec59e848111e812538c5aa7ee5f56247fc9b3ef310c8f2215->leave($__internal_685000e8c9dba6fec59e848111e812538c5aa7ee5f56247fc9b3ef310c8f2215_prof);

        
        $__internal_75f4ef706bacae942d03fbaa3fa6109dfd48b1b96fab488affb48e161dc7b4ff->leave($__internal_75f4ef706bacae942d03fbaa3fa6109dfd48b1b96fab488affb48e161dc7b4ff_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_137246a3e0c8772ba95d6136b9300e053dd83099c728f6a4040a3f045238b8ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_137246a3e0c8772ba95d6136b9300e053dd83099c728f6a4040a3f045238b8ab->enter($__internal_137246a3e0c8772ba95d6136b9300e053dd83099c728f6a4040a3f045238b8ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_98648955194a152b52ff7a4ddee68a361301561c59cc6e3335dbb5d2181f40be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98648955194a152b52ff7a4ddee68a361301561c59cc6e3335dbb5d2181f40be->enter($__internal_98648955194a152b52ff7a4ddee68a361301561c59cc6e3335dbb5d2181f40be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_98648955194a152b52ff7a4ddee68a361301561c59cc6e3335dbb5d2181f40be->leave($__internal_98648955194a152b52ff7a4ddee68a361301561c59cc6e3335dbb5d2181f40be_prof);

        
        $__internal_137246a3e0c8772ba95d6136b9300e053dd83099c728f6a4040a3f045238b8ab->leave($__internal_137246a3e0c8772ba95d6136b9300e053dd83099c728f6a4040a3f045238b8ab_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_68020ebdd7d1b6555678614231c9b688f6fefdfbaa96793c489104bcd08c02f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68020ebdd7d1b6555678614231c9b688f6fefdfbaa96793c489104bcd08c02f5->enter($__internal_68020ebdd7d1b6555678614231c9b688f6fefdfbaa96793c489104bcd08c02f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_06667e3b6a18a0b582630c7d8052e2467cdacd39153e04cbb366a1fa2e957dc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06667e3b6a18a0b582630c7d8052e2467cdacd39153e04cbb366a1fa2e957dc7->enter($__internal_06667e3b6a18a0b582630c7d8052e2467cdacd39153e04cbb366a1fa2e957dc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_06667e3b6a18a0b582630c7d8052e2467cdacd39153e04cbb366a1fa2e957dc7->leave($__internal_06667e3b6a18a0b582630c7d8052e2467cdacd39153e04cbb366a1fa2e957dc7_prof);

        
        $__internal_68020ebdd7d1b6555678614231c9b688f6fefdfbaa96793c489104bcd08c02f5->leave($__internal_68020ebdd7d1b6555678614231c9b688f6fefdfbaa96793c489104bcd08c02f5_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c5071f1198b3eb9bf66b9b0fc20a9678856a82acaf134325387437db9c22c29d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5071f1198b3eb9bf66b9b0fc20a9678856a82acaf134325387437db9c22c29d->enter($__internal_c5071f1198b3eb9bf66b9b0fc20a9678856a82acaf134325387437db9c22c29d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_0588279c08e7b5ffcb4a7b06673ec765b69b7c7c7fa2aa9d51bd17c3ed1b7be2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0588279c08e7b5ffcb4a7b06673ec765b69b7c7c7fa2aa9d51bd17c3ed1b7be2->enter($__internal_0588279c08e7b5ffcb4a7b06673ec765b69b7c7c7fa2aa9d51bd17c3ed1b7be2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_0588279c08e7b5ffcb4a7b06673ec765b69b7c7c7fa2aa9d51bd17c3ed1b7be2->leave($__internal_0588279c08e7b5ffcb4a7b06673ec765b69b7c7c7fa2aa9d51bd17c3ed1b7be2_prof);

        
        $__internal_c5071f1198b3eb9bf66b9b0fc20a9678856a82acaf134325387437db9c22c29d->leave($__internal_c5071f1198b3eb9bf66b9b0fc20a9678856a82acaf134325387437db9c22c29d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
