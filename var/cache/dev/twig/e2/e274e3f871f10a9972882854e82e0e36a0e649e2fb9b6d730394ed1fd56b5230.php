<?php

/* :security:login.html.twig */
class __TwigTemplate_271e3aeb18937000993ed08a309ae349fe67259593de6595d398106484e98067 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":security:login.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b52600990f15a481a85545816369416443c9f9af01ef3646e9a8447ba0680bef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b52600990f15a481a85545816369416443c9f9af01ef3646e9a8447ba0680bef->enter($__internal_b52600990f15a481a85545816369416443c9f9af01ef3646e9a8447ba0680bef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":security:login.html.twig"));

        $__internal_6b7ba7eb681c6baf567153e0dad7df7672958c2e0540c29f406f7ea909eecf94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b7ba7eb681c6baf567153e0dad7df7672958c2e0540c29f406f7ea909eecf94->enter($__internal_6b7ba7eb681c6baf567153e0dad7df7672958c2e0540c29f406f7ea909eecf94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b52600990f15a481a85545816369416443c9f9af01ef3646e9a8447ba0680bef->leave($__internal_b52600990f15a481a85545816369416443c9f9af01ef3646e9a8447ba0680bef_prof);

        
        $__internal_6b7ba7eb681c6baf567153e0dad7df7672958c2e0540c29f406f7ea909eecf94->leave($__internal_6b7ba7eb681c6baf567153e0dad7df7672958c2e0540c29f406f7ea909eecf94_prof);

    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        $__internal_24e505d0e3bf44931a17a0e6de314c8fbcc614405495a4ca71ba68f7c29b83e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24e505d0e3bf44931a17a0e6de314c8fbcc614405495a4ca71ba68f7c29b83e7->enter($__internal_24e505d0e3bf44931a17a0e6de314c8fbcc614405495a4ca71ba68f7c29b83e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        $__internal_491769316dee558c7f5f8c7d7bdec9f26e6c6fbf7843274f4a83a58fde87df6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_491769316dee558c7f5f8c7d7bdec9f26e6c6fbf7843274f4a83a58fde87df6e->enter($__internal_491769316dee558c7f5f8c7d7bdec9f26e6c6fbf7843274f4a83a58fde87df6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        // line 4
        echo "        <div>
            <a class=\"animated bounceInLeft navbar-brand\" href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Sabores del Sur</a>
            <div class=\"container\">
                <div class=\"col-lg-offset-10 col-md-offset-10 col-sm-offset-10\">
            <ul class=\"hidden-xs nav navbar-nav main-nav clear\">
                <li><a class=\"navactive color_animation\">Ingreso</a></li>
                <!--Pregunto si esta logueado o no para iniciar o cerrar sesion-->
                ";
        // line 11
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 12
            echo "                    <li><a></a></li>
                    <li><a class=\"color_animation\" href=\"";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
            echo "\">Volver a Inicio</a></li>
                    <li><a class=\"color_animation\" href=\"";
            // line 14
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">Cerrar Sesión</a></li>
                ";
        }
        // line 16
        echo "            </ul>
           </div>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
";
        // line 27
        echo "    ";
        
        $__internal_491769316dee558c7f5f8c7d7bdec9f26e6c6fbf7843274f4a83a58fde87df6e->leave($__internal_491769316dee558c7f5f8c7d7bdec9f26e6c6fbf7843274f4a83a58fde87df6e_prof);

        
        $__internal_24e505d0e3bf44931a17a0e6de314c8fbcc614405495a4ca71ba68f7c29b83e7->leave($__internal_24e505d0e3bf44931a17a0e6de314c8fbcc614405495a4ca71ba68f7c29b83e7_prof);

    }

    // line 29
    public function block_body($context, array $blocks = array())
    {
        $__internal_a465d6438f948cd1e467624a22b3b4379601fb28ff3cafbe9f82dc99c39acef5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a465d6438f948cd1e467624a22b3b4379601fb28ff3cafbe9f82dc99c39acef5->enter($__internal_a465d6438f948cd1e467624a22b3b4379601fb28ff3cafbe9f82dc99c39acef5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e534aaa3da3f6accfe2f0d664f7192ab4c9673268aaea0403bfbb38a5c21d4a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e534aaa3da3f6accfe2f0d664f7192ab4c9673268aaea0403bfbb38a5c21d4a7->enter($__internal_e534aaa3da3f6accfe2f0d664f7192ab4c9673268aaea0403bfbb38a5c21d4a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 30
        echo "    <div id=\"top\" class=\"starter_container_login bg\">
        <div class=\"follow_container\">
            <div class=\"col-md-6 col-md-offset-1\">
                <h2 class=\"color-login\">Ingreso</h2><br>
                ";
        // line 34
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 34, $this->getSourceContext()); })())) {
            // line 35
            echo "                    <div class=\"alert alert-danger\">
                        ";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 36, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 36, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 39
        echo "
                ";
        // line 40
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formLogin"]) || array_key_exists("formLogin", $context) ? $context["formLogin"] : (function () { throw new Twig_Error_Runtime('Variable "formLogin" does not exist.', 40, $this->getSourceContext()); })()), 'form_start');
        echo "
                ";
        // line 41
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formLogin"]) || array_key_exists("formLogin", $context) ? $context["formLogin"] : (function () { throw new Twig_Error_Runtime('Variable "formLogin" does not exist.', 41, $this->getSourceContext()); })()), "_username", array()), 'row', array("label" => "Usuario", "label_attr" => array("class" => "color-login")));
        // line 44
        echo "
                ";
        // line 45
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formLogin"]) || array_key_exists("formLogin", $context) ? $context["formLogin"] : (function () { throw new Twig_Error_Runtime('Variable "formLogin" does not exist.', 45, $this->getSourceContext()); })()), "_password", array()), 'row', array("label" => "Contraseña", "label_attr" => array("class" => "color-login")));
        // line 48
        echo "
                <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn btn-success\">Volver  <span class=\"fa fa-arrow-left\"></span></a>
                <button type=\"submit\" class=\"btn btn-success\" formnovalidate>Ingresar  <span class=\"fa fa-lock\"></span></button>
                ";
        // line 51
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formLogin"]) || array_key_exists("formLogin", $context) ? $context["formLogin"] : (function () { throw new Twig_Error_Runtime('Variable "formLogin" does not exist.', 51, $this->getSourceContext()); })()), 'form_end');
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_e534aaa3da3f6accfe2f0d664f7192ab4c9673268aaea0403bfbb38a5c21d4a7->leave($__internal_e534aaa3da3f6accfe2f0d664f7192ab4c9673268aaea0403bfbb38a5c21d4a7_prof);

        
        $__internal_a465d6438f948cd1e467624a22b3b4379601fb28ff3cafbe9f82dc99c39acef5->leave($__internal_a465d6438f948cd1e467624a22b3b4379601fb28ff3cafbe9f82dc99c39acef5_prof);

    }

    public function getTemplateName()
    {
        return ":security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 51,  137 => 49,  134 => 48,  132 => 45,  129 => 44,  127 => 41,  123 => 40,  120 => 39,  114 => 36,  111 => 35,  109 => 34,  103 => 30,  94 => 29,  84 => 27,  76 => 16,  71 => 14,  67 => 13,  64 => 12,  62 => 11,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig'%}

    {% block banner %}
        <div>
            <a class=\"animated bounceInLeft navbar-brand\" href=\"{{ path('homepage') }}\">Sabores del Sur</a>
            <div class=\"container\">
                <div class=\"col-lg-offset-10 col-md-offset-10 col-sm-offset-10\">
            <ul class=\"hidden-xs nav navbar-nav main-nav clear\">
                <li><a class=\"navactive color_animation\">Ingreso</a></li>
                <!--Pregunto si esta logueado o no para iniciar o cerrar sesion-->
                {% if is_granted('ROLE_USER') %}
                    <li><a></a></li>
                    <li><a class=\"color_animation\" href=\"{{ path('homepage') }}\">Volver a Inicio</a></li>
                    <li><a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a></li>
                {% endif %}
            </ul>
           </div>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
{#        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav main-nav  clear navbar-right \">
                <li><a href=\"#\" class=\"navactive color_animation\">Ingreso</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->#}
    {% endblock %}

{% block body %}
    <div id=\"top\" class=\"starter_container_login bg\">
        <div class=\"follow_container\">
            <div class=\"col-md-6 col-md-offset-1\">
                <h2 class=\"color-login\">Ingreso</h2><br>
                {% if error %}
                    <div class=\"alert alert-danger\">
                        {{ error.messageKey|trans(error.messageData, 'security') }}
                    </div>
                {% endif %}

                {{ form_start(formLogin) }}
                {{ form_row(formLogin._username, {
                    'label': 'Usuario',
                    'label_attr': {'class': 'color-login'}
                }) }}
                {{ form_row(formLogin._password, {
                    'label': 'Contraseña',
                    'label_attr': {'class': 'color-login'}
                }) }}
                <a href=\"{{ path('homepage') }}\" class=\"btn btn-success\">Volver  <span class=\"fa fa-arrow-left\"></span></a>
                <button type=\"submit\" class=\"btn btn-success\" formnovalidate>Ingresar  <span class=\"fa fa-lock\"></span></button>
                {{ form_end(formLogin) }}
            </div>
        </div>
    </div>
{% endblock %}", ":security:login.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/app/Resources/views/security/login.html.twig");
    }
}
