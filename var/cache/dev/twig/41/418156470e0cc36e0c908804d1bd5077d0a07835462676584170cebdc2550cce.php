<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_2bd1971f019d8eea800ce7a73e8ac540c5663e6a19090f7624666c3c5ca41039 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85a2b8744d5155bdf37e93784480221544b1bd784dd8a7e14f3c8dc1bfb13814 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85a2b8744d5155bdf37e93784480221544b1bd784dd8a7e14f3c8dc1bfb13814->enter($__internal_85a2b8744d5155bdf37e93784480221544b1bd784dd8a7e14f3c8dc1bfb13814_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_a252230e5677365c6e1fa40b7eb6b8f2b896a7acbb956bdcc1e2a5024b3a0e08 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a252230e5677365c6e1fa40b7eb6b8f2b896a7acbb956bdcc1e2a5024b3a0e08->enter($__internal_a252230e5677365c6e1fa40b7eb6b8f2b896a7acbb956bdcc1e2a5024b3a0e08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_85a2b8744d5155bdf37e93784480221544b1bd784dd8a7e14f3c8dc1bfb13814->leave($__internal_85a2b8744d5155bdf37e93784480221544b1bd784dd8a7e14f3c8dc1bfb13814_prof);

        
        $__internal_a252230e5677365c6e1fa40b7eb6b8f2b896a7acbb956bdcc1e2a5024b3a0e08->leave($__internal_a252230e5677365c6e1fa40b7eb6b8f2b896a7acbb956bdcc1e2a5024b3a0e08_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
