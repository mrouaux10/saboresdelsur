<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_ba0015af671dcd9a0058b3aad5a1aa08a02e444e5cbf99e6ad679838c94509b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_deba05cf5f98e234a3961a725cb52aa7f0dd9cb94aa6a53f2d136638ff4b5d2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_deba05cf5f98e234a3961a725cb52aa7f0dd9cb94aa6a53f2d136638ff4b5d2d->enter($__internal_deba05cf5f98e234a3961a725cb52aa7f0dd9cb94aa6a53f2d136638ff4b5d2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_2cde5a982e29fa40d3ac9b21face9121335bf0ece3a580fca974471a644bd1cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2cde5a982e29fa40d3ac9b21face9121335bf0ece3a580fca974471a644bd1cf->enter($__internal_2cde5a982e29fa40d3ac9b21face9121335bf0ece3a580fca974471a644bd1cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_deba05cf5f98e234a3961a725cb52aa7f0dd9cb94aa6a53f2d136638ff4b5d2d->leave($__internal_deba05cf5f98e234a3961a725cb52aa7f0dd9cb94aa6a53f2d136638ff4b5d2d_prof);

        
        $__internal_2cde5a982e29fa40d3ac9b21face9121335bf0ece3a580fca974471a644bd1cf->leave($__internal_2cde5a982e29fa40d3ac9b21face9121335bf0ece3a580fca974471a644bd1cf_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
