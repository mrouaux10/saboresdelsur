<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_5402c722c794c463359d3d752edcace91eff4c68cbf5559329ef5dc975674434 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6a1a4bc4b6103282ac3b4573069754bec5e342daec764a111ccad93d98956ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6a1a4bc4b6103282ac3b4573069754bec5e342daec764a111ccad93d98956ae->enter($__internal_a6a1a4bc4b6103282ac3b4573069754bec5e342daec764a111ccad93d98956ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_06a8632b9bf1085a67656ac18a0471a0b524d88278f4101e00f668afabdff86c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06a8632b9bf1085a67656ac18a0471a0b524d88278f4101e00f668afabdff86c->enter($__internal_06a8632b9bf1085a67656ac18a0471a0b524d88278f4101e00f668afabdff86c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_a6a1a4bc4b6103282ac3b4573069754bec5e342daec764a111ccad93d98956ae->leave($__internal_a6a1a4bc4b6103282ac3b4573069754bec5e342daec764a111ccad93d98956ae_prof);

        
        $__internal_06a8632b9bf1085a67656ac18a0471a0b524d88278f4101e00f668afabdff86c->leave($__internal_06a8632b9bf1085a67656ac18a0471a0b524d88278f4101e00f668afabdff86c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
