<?php

/* index/index.html.twig */
class __TwigTemplate_71c2b80ec3a2a589b95f13e23f5feda3921357dc9b1e3712846a5d2e31a91140 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "index/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'banner' => array($this, 'block_banner'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15a428b46fcded93f7398c702127a4f49834ad70ec19c181874d250488d19b6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15a428b46fcded93f7398c702127a4f49834ad70ec19c181874d250488d19b6e->enter($__internal_15a428b46fcded93f7398c702127a4f49834ad70ec19c181874d250488d19b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $__internal_139746c65d7a8d5f54d3f3fdeb9230109e293658c6f169520749598b367918f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_139746c65d7a8d5f54d3f3fdeb9230109e293658c6f169520749598b367918f0->enter($__internal_139746c65d7a8d5f54d3f3fdeb9230109e293658c6f169520749598b367918f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15a428b46fcded93f7398c702127a4f49834ad70ec19c181874d250488d19b6e->leave($__internal_15a428b46fcded93f7398c702127a4f49834ad70ec19c181874d250488d19b6e_prof);

        
        $__internal_139746c65d7a8d5f54d3f3fdeb9230109e293658c6f169520749598b367918f0->leave($__internal_139746c65d7a8d5f54d3f3fdeb9230109e293658c6f169520749598b367918f0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_bf618b7cf086ea4d005533f9c3915c055cb63f2d4243e7ab0b1c799cd9d5c4e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf618b7cf086ea4d005533f9c3915c055cb63f2d4243e7ab0b1c799cd9d5c4e5->enter($__internal_bf618b7cf086ea4d005533f9c3915c055cb63f2d4243e7ab0b1c799cd9d5c4e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b8082c0e53fd2c63af75da8ae11d6ea804d4f8a934828c84366a5e387b010808 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8082c0e53fd2c63af75da8ae11d6ea804d4f8a934828c84366a5e387b010808->enter($__internal_b8082c0e53fd2c63af75da8ae11d6ea804d4f8a934828c84366a5e387b010808_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sabores del Sur";
        
        $__internal_b8082c0e53fd2c63af75da8ae11d6ea804d4f8a934828c84366a5e387b010808->leave($__internal_b8082c0e53fd2c63af75da8ae11d6ea804d4f8a934828c84366a5e387b010808_prof);

        
        $__internal_bf618b7cf086ea4d005533f9c3915c055cb63f2d4243e7ab0b1c799cd9d5c4e5->leave($__internal_bf618b7cf086ea4d005533f9c3915c055cb63f2d4243e7ab0b1c799cd9d5c4e5_prof);

    }

    // line 5
    public function block_banner($context, array $blocks = array())
    {
        $__internal_bd25a061dc363ab7fe9183c04ce42fff93270207a6b39f438d540be7136f1011 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd25a061dc363ab7fe9183c04ce42fff93270207a6b39f438d540be7136f1011->enter($__internal_bd25a061dc363ab7fe9183c04ce42fff93270207a6b39f438d540be7136f1011_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        $__internal_49ba50839a8556b6fb8d6f1b94907756512b38b406ed598b83b1daab15bbe2bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49ba50839a8556b6fb8d6f1b94907756512b38b406ed598b83b1daab15bbe2bf->enter($__internal_49ba50839a8556b6fb8d6f1b94907756512b38b406ed598b83b1daab15bbe2bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        // line 6
        echo "
            <button class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navHeaderCollapse\">
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>

            <div class=\"collapse navbar-collapse navHeaderCollapse\">
                ";
        // line 14
        if ( !$this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 15
            echo "                    <a class=\"animated bounceInLeft navbar-brand\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
            echo "\">Sabores del Sur</a>
                ";
        } else {
            // line 17
            echo "                    <a class=\"navbar-brand\">Sabores del Sur</a>
                ";
        }
        // line 19
        echo "                <div class=\"container\">
                    <div class=\"col-lg-offset-6 col-md-offset-5 col-sm-offset-3\">
                        <ul class=\"nav navbar-nav main-nav clear\">
                            <li><a href=\"#top\" class=\"active color_animation\">Principal</a></li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle color_animation\" data-toggle=\"dropdown\">Productos <b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu color_animation\" style=\"background: #222\">
                                    <li><a class=\"color_animation\" href=\"#pricing\">Catálogo de Exquisiteses</a></li>
                                    <li><a class=\"color_animation\" href=\"#beer\">Nuestras Especialidades</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle color_animation\" data-toggle=\"dropdown\">Nosotros <b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu color_animation\" style=\"background: #222\">
                                    <li><a class=\"color_animation\" href=\"#story\">Sobre nosotros </a></li>
                                    <li><a class=\"color_animation\" href=\"#contact\">Contacto</a></li>
                                </ul>
                            </li>
                            <li><a href=\"#reservation\" class=\"color_animation\">Pedidos</a></li>
                            ";
        // line 38
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 39
            echo "                                <li><a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">Cerrar Sesión</a></li>
                            ";
        } else {
            // line 41
            echo "                                <li><a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
            echo "\">Ingresar</a></li>
                            ";
        }
        // line 43
        echo "                        </ul>
                    </div>
                </div>

            </div>

";
        // line 78
        echo "
            <!-- ============ Mensaje de aviso exito al cargar imagen ============= -->
                        ";
        // line 80
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 80, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeExito"), "method");
        // line 81
        echo "                        ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 81, $this->getSourceContext()); })()))) {
            // line 82
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 82, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 83
                echo "                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                    ";
                // line 84
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "                        ";
        }
        // line 88
        echo "            <!-- ============ Mensaje de error limite de tamaño excedido ============= -->
                        ";
        // line 89
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 89, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeLimiteTamaño"), "method");
        // line 90
        echo "                        ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 90, $this->getSourceContext()); })()))) {
            // line 91
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 91, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 92
                echo "                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-danger\">
                                    ";
                // line 93
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "                        ";
        }
        // line 97
        echo "            <!-- ============ Mensaje de error tipo de archivo no reconocido ============= -->
                        ";
        // line 98
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 98, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeTipoArchivo"), "method");
        // line 99
        echo "                        ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 99, $this->getSourceContext()); })()))) {
            // line 100
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 100, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 101
                echo "                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-danger\">
                                    ";
                // line 102
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 105
            echo "                        ";
        }
        // line 106
        echo "            <!-- ============ Mensaje de consulta de pedido ============= -->
                    ";
        // line 107
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 107, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeOrdenPedidoExito"), "method");
        // line 108
        echo "                    ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 108, $this->getSourceContext()); })()))) {
            // line 109
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 109, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 110
                echo "                            <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                ";
                // line 111
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                            </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "                    ";
        }
        // line 115
        echo "            <!-- ============ Mensaje de contacto ============= -->
                    ";
        // line 116
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 116, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeContactoExito"), "method");
        // line 117
        echo "                    ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 117, $this->getSourceContext()); })()))) {
            // line 118
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 118, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 119
                echo "                            <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                ";
                // line 120
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                            </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo "                    ";
        }
        // line 124
        echo "            <!-- ============ Mensaje de aviso exito al cargar imagen ============= -->
            ";
        // line 125
        $context["flashbag_notices"] = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 125, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "get", array(0 => "mensajeBorradoArchivo"), "method");
        // line 126
        echo "            ";
        if ( !twig_test_empty((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 126, $this->getSourceContext()); })()))) {
            // line 127
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashbag_notices"]) || array_key_exists("flashbag_notices", $context) ? $context["flashbag_notices"] : (function () { throw new Twig_Error_Runtime('Variable "flashbag_notices" does not exist.', 127, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["mensaje"]) {
                // line 128
                echo "                    <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                        ";
                // line 129
                echo twig_escape_filter($this->env, $context["mensaje"], "html", null, true);
                echo "
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mensaje'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "            ";
        }
        // line 133
        echo "        ";
        
        $__internal_49ba50839a8556b6fb8d6f1b94907756512b38b406ed598b83b1daab15bbe2bf->leave($__internal_49ba50839a8556b6fb8d6f1b94907756512b38b406ed598b83b1daab15bbe2bf_prof);

        
        $__internal_bd25a061dc363ab7fe9183c04ce42fff93270207a6b39f438d540be7136f1011->leave($__internal_bd25a061dc363ab7fe9183c04ce42fff93270207a6b39f438d540be7136f1011_prof);

    }

    // line 135
    public function block_body($context, array $blocks = array())
    {
        $__internal_947e18ba10c53446282036cbd40869d4d2fd97197beb78f5a12f7f9d74b57a4b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_947e18ba10c53446282036cbd40869d4d2fd97197beb78f5a12f7f9d74b57a4b->enter($__internal_947e18ba10c53446282036cbd40869d4d2fd97197beb78f5a12f7f9d74b57a4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_20534025ce8ca1d8552809be0dfef96c7bf7eb77aaa784f01e62ee30f329bec1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20534025ce8ca1d8552809be0dfef96c7bf7eb77aaa784f01e62ee30f329bec1->enter($__internal_20534025ce8ca1d8552809be0dfef96c7bf7eb77aaa784f01e62ee30f329bec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 136
        echo "        <div class=\"starter_container bg\">
            <div class=\"follow_container\">
                <div class=\"col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12\">
                    ";
        // line 139
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 140
            echo "                        <a class=\"animated bounceInLeft navbar-brand\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_profile");
            echo "\">Administrar</a>
                    ";
        }
        // line 141
        echo "<br><br><br><br>
                    <h2 class=\"animated swing navbar-brand-bigger\">Repostería</h2><br><br>
                    <hr class=\"animated bounceInLeft\">
                    <h2 class=\"animated bounceInDown white second-title\">\" La mejor en la ciudad \"</h2>
                    <hr class=\"animated bounceInRight\">
                </div>
            </div>
        </div>

        <!-- ============ About Us ============= -->

        <section id=\"story\" class=\"description_content scrollflow -slide-right -opacity\">
            <div class=\"text-content container\">
                <div class=\"col-md-6\">
                    <h1>Conócenos</h1>
                    <div class=\"fa fa-cutlery fa-2x\"></div>
                    <p class=\"desc-text\">Somos una familia bien dispuesta cuyo propósito es comenzar a emerger en este hermoso emprendimiento que busca convertir cada segundo en una verdadera aventura, no solo para el paladar de aquellos que lo prueben, sino también para la vida. Creemos que la cocina es un lenguaje mediante el cual se puede expresar armonia, creatividad, felicidad, poesia, magia, humor, provocación. Queda en ustedes que tipo emoción quieran generar! Sin más, los invito a ser parte de este sueño!</p>
                </div>

                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"hidden-xs col-md-6\">
                            <div class=\"img-section\">
                                <img src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21192483_1679525915451816_4323066963082020934_n.jpg"), "html", null, true);
        echo "\" width=\"250\" height=\"220\">
                                <img src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21150099_1679438395460568_4188395351542253078_n.jpg"), "html", null, true);
        echo "\" width=\"250\" height=\"220\">
                                <div class=\"img-section-space\"></div>
                                <img src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21150057_1679527285451679_6146148214145008711_n.jpg"), "html", null, true);
        echo "\" width=\"250\" height=\"220\">
                                <img src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/20180318_130317.jpg"), "html", null, true);
        echo "\" width=\"250\" height=\"220\">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"hidden-lg hidden-md hidden-sm col-xs-6\">
                            <img src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21192483_1679525915451816_4323066963082020934_n.jpg"), "html", null, true);
        echo "\"  width=\"100%\">
                            <div class=\"img-section-space\"></div>
                            <img src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21150099_1679438395460568_4188395351542253078_n.jpg"), "html", null, true);
        echo "\" width=\"100%\">
                            <div class=\"img-section-space\"></div>
                        </div>
                        <div class=\"hidden-lg hidden-md hidden-sm col-md-6 col-xs-6\">
                            <img src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/21150057_1679527285451679_6146148214145008711_n.jpg"), "html", null, true);
        echo "\"  width=\"97%\">
                            <div class=\"img-section-space\"></div>
                            <img src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/Other/20180318_130317.jpg"), "html", null, true);
        echo "\" width=\"97%\">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ============ Galeria de fotos ============= -->
        <section class=\"description_content\">
            <div class=\"fixed-bg-pricing\">
                <br><br>
                <h2 class=\"navbar-brand-bigger\">Precio único</h2>
            </div>
            <br><br>
        <div class=\"container\">
            <div class=\"row\">
                <hr>
                    <div id=\"pricing\" class=\"col-md-12\">
                        <h2 class=\"title\"><em>Mira nuestros productos y consulta tu pedido sin cargo!</em></h2>
                    </div>
                <hr>
                    <div class=\"col-md-12\">
                        <p style=\"text-align: center\" class=\"desc-text\"><em>Seleccione un elemento para realizar una consulta sin cargo alguno.</em></p><br>
                    </div>
                <div>
                    <div align=\"center\">
                        <button class=\"btn btn-success filter-button\" data-filter=\"all\">Todos</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"hdpe\">Tortas</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"sprinkle\">Tartas</button>
                        <br class=\"hidden-lg hidden-md\">
                        <br class=\"hidden-lg hidden-md\">
                        <button class=\"btn btn-info filter-button\" data-filter=\"spray\">Mesas Dulces</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"irrigation\">Variedades</button>
                    </div>
                </div>
                <div class=\"col-md-12\">
                    <div class=\"col-md-6 col-md-offset-3\">
                        <label  style=\"visibility: hidden\" id=\"codigoPedido\">Código de Pedido: </label>
                    </div>
                </div>

                <!-- Tortas -->
                    ";
        // line 226
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tortas"]) || array_key_exists("tortas", $context) ? $context["tortas"] : (function () { throw new Twig_Error_Runtime('Variable "tortas" does not exist.', 226, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["torta"]) {
            // line 227
            echo "                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter hdpe\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
            // line 228
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["torta"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["torta"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["torta"], "tituloDescripcion", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["torta"], "descripcion", array()), "html", null, true);
            echo "'), fijarCodigo('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["torta"], "codigo", array()), "html", null, true);
            echo "')\" alt=\"Card image cap\">
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['torta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 231
        echo "                        <!-- Tartas -->
                    ";
        // line 232
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tartas"]) || array_key_exists("tartas", $context) ? $context["tartas"] : (function () { throw new Twig_Error_Runtime('Variable "tartas" does not exist.', 232, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["tarta"]) {
            // line 233
            echo "                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter sprinkle\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
            // line 234
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["tarta"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["tarta"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tarta"], "tituloDescripcion", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tarta"], "descripcion", array()), "html", null, true);
            echo "'), fijarCodigo('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tarta"], "codigo", array()), "html", null, true);
            echo "')\" alt=\"Card image cap\">
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tarta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 237
        echo "                        <!-- Mesas dulces -->
                    ";
        // line 238
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mesasDulces"]) || array_key_exists("mesasDulces", $context) ? $context["mesasDulces"] : (function () { throw new Twig_Error_Runtime('Variable "mesasDulces" does not exist.', 238, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["mesaDulce"]) {
            // line 239
            echo "                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter spray\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
            // line 240
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["mesaDulce"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["mesaDulce"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["mesaDulce"], "tituloDescripcion", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["mesaDulce"], "descripcion", array()), "html", null, true);
            echo "'), fijarCodigo('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["mesaDulce"], "codigo", array()), "html", null, true);
            echo "')\" alt=\"Card image cap\">
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mesaDulce'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 243
        echo "                        <!-- Variedades -->
                    ";
        // line 244
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["variedades"]) || array_key_exists("variedades", $context) ? $context["variedades"] : (function () { throw new Twig_Error_Runtime('Variable "variedades" does not exist.', 244, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["variedad"]) {
            // line 245
            echo "                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter irrigation\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/";
            // line 246
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["variedad"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["variedad"], "getImage", array()), "getOriginalName", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["variedad"], "tituloDescripcion", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["variedad"], "descripcion", array()), "html", null, true);
            echo "'), fijarCodigo('";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["variedad"], "codigo", array()), "html", null, true);
            echo "')\" alt=\"Card image cap\">
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variedad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 249
        echo "            </div>
        </div>
        </section>

        <!-- ============ Nuestras Tortas  ============= -->
        <section id =\"beer\" class=\"description_content\">
            <div  class=\"fixed-bg-specialty\">
                <br><br>
                <h2 class=\"navbar-brand-bigger\">Nuestra especialidad</h2>
            </div>
            <div class=\"text-content container\">

                <div class=\"col-md-6 col-md-offset-1\">
                    <h1>Nuestras tortas</h1>
                    <div class=\"fa fa-birthday-cake fa-3x\"></div>
                        <p class=\"desc-text\">Todos nuestros bizcochuelos son caseros elaborados con ingredientes de la mejor calidad, por eso se ven los resultados al deleitar el paladar de nuestrs clientes.</p>
                        <br>
                        <p class=\"desc-text\">Te invitamos a probar nuestra exquisita Torta Du Soleil 2.0 y nos cuentes que te parece!</p>
                </div>
                <div class=\"col-md-5 col-\">
                    <div class=\"img-section\">
                        <img src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/especialidad.jpeg"), "html", null, true);
        echo "\" width=\"80%\">
                    </div>
                </div>
            </div>
            <br>
        </section>

        <!-- ============ Reservation  ============= -->

        <section class=\"description_content\">
            <div class=\"fixed-bg-order scrollflow -slide-bottom -opacity\">
                <br><br><br><br>
                <h2 class=\"navbar-brand-bigger-pedido\">Consulta por tu pedido!</h2>
            </div>
            <div class=\"text-content container\">
                <div class=\"inner contact\">
                    <!-- Form Area -->
                    <div id=\"reservation\" class=\"contact-form\">
                        <!-- Form -->
                            <hr>
                            <div class=\"row scrollflow -slide-bottom -opacity\">
                                <div class=\"col-md-12\">
                                    <h2 class=\"second-title\"> Completa tus datos para consultar por tu pedido!</h2>
                                </div>
                            </div>
                            <hr>
                                <div class=\"text-content container scrollflow -slide-top -opacity\">
                                    <div class=\"col-md-5\">
                                        <div>
                                            ";
        // line 299
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 299, $this->getSourceContext()); })()), 'form_start');
        echo "
                                            ";
        // line 300
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 300, $this->getSourceContext()); })()), "name", array()), 'row', array("label" => "Nombre completo"));
        // line 302
        echo "
                                            ";
        // line 303
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 303, $this->getSourceContext()); })()), "email", array()), 'row');
        echo "
                                            ";
        // line 304
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 304, $this->getSourceContext()); })()), "phone", array()), 'row', array("label" => "Teléfono"));
        // line 306
        echo "
                                            ";
        // line 307
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 307, $this->getSourceContext()); })()), "dateExpected", array()), 'row', array("label" => "Fecha en la que necesito este producto"));
        // line 309
        echo "
                                            ";
        // line 310
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 310, $this->getSourceContext()); })()), "orderCode", array()), 'row', array("label" => false, "id" => "codPedido", "attr" => array("readonly" => "true")));
        // line 314
        echo "
                                        </div>
                                    </div>
                                    <br class=\"hidden-xs\">
                                    <div class=\"col-md-6 col-md-offset-1\">
                                        <p class=\"text-info\">Ya casi! Sólo falta una descripción...</p>
                                        <div class=\"fa fa-thumbs-o-up fa-4x\"></div>
                                        <p class=\"desc-text\">Cuéntanos cada detalle que deseas en tu pedido, nosotros te responderemos al instante sobre nuestros precios y recomendaciones sin ningún compromiso de compra.</p>
                                        <br><br>
                                        ";
        // line 323
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 323, $this->getSourceContext()); })()), "descripcion", array()), 'row', array("label" => "Descripción", "attr" => array("placeholder" => "¿Cómo decorarías este producto?Cuentanos... :)")));
        // line 326
        echo "
                                        <div class=\"col-lg-offset-8\">
                                            <h6>(Cant. Pisos; Colores; Extras).</h6>
                                        </div>
                                    </div>
                                    <div class=\"col-md-10 col-lg-offset-1\" >
                                    <button onclick=\"location.href='#reservation';\" type=\"submit\" class=\"btn btn-success btn-lg btn-block\" formnovalidate>Consultar por este Pedido!</button>

                                    </div>
                                        ";
        // line 335
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formOrder"]) || array_key_exists("formOrder", $context) ? $context["formOrder"] : (function () { throw new Twig_Error_Runtime('Variable "formOrder" does not exist.', 335, $this->getSourceContext()); })()), 'form_end');
        echo "
                                </div>
                            </div>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>

        <!-- ============ Social Section  ============= -->

        <section class=\"social_connect scrollflow -slide-top -opacity\">
            <div class=\"text-content container\">
                <div class=\"col-md-6\">
                    <span class=\"social_heading\">Seguinos</span>
                    <ul class=\"social_icons\">
                        <li><a class=\"icon-twitter color_animation\" href=\"#\" target=\"_blank\"></a></li>
                        <li><a class=\"icon-github color_animation\" href=\"#\" target=\"_blank\"></a></li>
                        <li><a class=\"fa fa-facebook color_animation\" href=\"https://www.facebook.com/taniasantossaboresdelsur/\" target=\"_blank\"></a></li>
                        <li><a class=\"icon-mail color_animation\" href=\"#\"></a></li>
                    </ul>
                </div>
                <div class=\"col-md-4\">
                    <span class=\"social_heading\">O Llamanos</span>
                    <span class=\"social_info\"><a class=\"color_animation\" href=\"tel:156-246-712\">(0351)156-246-712</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Contact Section  ============= -->
        <section class=\"description_content\">
            <div id=\"contact\" class=\"text-content container\">
                <div class=\"inner contact\">
                    <!-- Form Area -->
                    <div class=\"contact-form\">
                        <!-- Form -->
                        <hr>
                        <div class=\"row scrollflow -slide-bottom -opacity\">
                            <div class=\"col-md-12\">
                                <h2 class=\"second-title\"> Tu opinión nos interesa, dejanos un comentario!</h2>
                            </div>
                        </div>
                        <hr>
                        <div class=\" row text-content container scrollflow -slide-top -opacity\">
                            <div class=\"col-md-5\">
                                <div>
                                    ";
        // line 380
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 380, $this->getSourceContext()); })()), 'form_start');
        echo "
                                    ";
        // line 381
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 381, $this->getSourceContext()); })()), "name", array()), 'row', array("label" => "Nombre completo"));
        // line 384
        echo "
                                    ";
        // line 385
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 385, $this->getSourceContext()); })()), "email", array()), 'row');
        echo "
                                    ";
        // line 386
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 386, $this->getSourceContext()); })()), "topic", array()), 'row', array("label" => "Tema"));
        // line 388
        echo "
                                </div>
                            </div>
                            <br>
                            <div class=\"col-md-6 col-md-offset-1\">
                                <div class=\"fa fa-thumbs-o-up fa-4x\"></div>
                                ";
        // line 394
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 394, $this->getSourceContext()); })()), "mensaje", array()), 'row', array("label" => "Mensaje"));
        // line 396
        echo "
                            </div>
                            <div class=\"col-md-10 col-lg-offset-1\" >
                                <button onclick=\"location.href='#contact';\" type=\"submit\" class=\"btn btn-success btn-lg btn-block\" formnovalidate>Enviar Mensaje!</button>
                                <br>
                            </div>
                            ";
        // line 402
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formContact"]) || array_key_exists("formContact", $context) ? $context["formContact"] : (function () { throw new Twig_Error_Runtime('Variable "formContact" does not exist.', 402, $this->getSourceContext()); })()), 'form_end');
        echo "
                        </div>
                    </div>
                </div><!-- End Contact Form Area -->
            </div><!-- End Inner -->
            </div>
        </section>


<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalTitle\" aria-hidden=\"true\">
<div class=\"modal-dialog\" role=\"document\">
<div class=\"modal-content\">
 <div class=\"modal-header\">
         <div class=\"row\">
             <div class=\"col-lg-11 col-md-10 col-sm-10 col-xs-9\">
                 <em><h5 class=\"modal-title\" id=\"myModalTitle\"><p style=\"font-size: 20px;\" id=\"textoTitulo\"></p></h5></em>
             </div>
             <div class=\"col-lg-1 col-md-2 col-sm-2 col-xs-3\">
                 <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                     <span aria-hidden=\"true\">&times;</span>
                 </button>
             </div>
         </div>
 </div>
 <div class=\"modal-body\">
     <div class=\"container\">
     <div class=\"row\">
         <div class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\">
             <em>
                 <p style=\"font-size: 13px;\" id=\"textoDescripcion\"></p>
                 <p style=\"font-size: 13px;\" class=\"desc-text\">Aclaración: Todos los rellenos descriptos pueden modificarse a gusto del cliente.</p>
             </em>
         </div>
     </div>
     <div class=\"row\">
         <div align=\"middle\" class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\" >
             <img id=\"imagenAExpandir\" style=\" width: 220px;\" alt=\"Card image cap\">
         </div>
     </div>
     </div>
 </div>
 <div class=\"modal-footer\">
     <div class=\"container\">
         <div class=\"row\">
             <div class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\">
                 <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cerrar</button>
                 <button type=\"button\" id=\"botonModal\" onclick=\"cerrarYPedir()\" class=\"btn btn-primary\">Esta es para mí, consultar!</button>
             </div>
         </div>
     </div>
 </div>
</div>
</div>
</div>

    <!-- ============ Footer Section  ============= -->
        <footer class=\"sub_footer\">
            <div class=\"container\">
                <div class=\"row\">
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-5\">
                            <p class=\"sub-footer-text text-center\">&copy; Sabores del Sur 2017</p>
                        </div>
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">
                            <p class=\"sub-footer-text text-center\">Volver al <a href=\"#top\">Inicio</a></p>
                        </div>
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-3\">
                            ";
        // line 469
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 470
            echo "                                <a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">Cerrar Sesión</a></li>
                            ";
        } else {
            // line 472
            echo "                                <a class=\"color_animation\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
            echo "\">Iniciar Sesión</a>
                            ";
        }
        // line 474
        echo "                    </div>
                </div>
            </div>
        </footer>

    ";
        
        $__internal_20534025ce8ca1d8552809be0dfef96c7bf7eb77aaa784f01e62ee30f329bec1->leave($__internal_20534025ce8ca1d8552809be0dfef96c7bf7eb77aaa784f01e62ee30f329bec1_prof);

        
        $__internal_947e18ba10c53446282036cbd40869d4d2fd97197beb78f5a12f7f9d74b57a4b->leave($__internal_947e18ba10c53446282036cbd40869d4d2fd97197beb78f5a12f7f9d74b57a4b_prof);

    }

    // line 481
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c99cc7a5844521f1c4dc4cef20e94d40a5dfd7accd6d448249335596005455ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c99cc7a5844521f1c4dc4cef20e94d40a5dfd7accd6d448249335596005455ec->enter($__internal_c99cc7a5844521f1c4dc4cef20e94d40a5dfd7accd6d448249335596005455ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_09028ed3a9da449265c665a2214ab30df0a1894e2be2a6ef3dd66f5080dad996 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09028ed3a9da449265c665a2214ab30df0a1894e2be2a6ef3dd66f5080dad996->enter($__internal_09028ed3a9da449265c665a2214ab30df0a1894e2be2a6ef3dd66f5080dad996_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 482
        echo "
        ";
        // line 483
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

        <script>
            function fijarCodigo(codigo) {
                document.getElementById(\"codigoPedido\").innerHTML = \"Código de Pedido:\" + codigo;
                \$('#codPedido').val(codigo);
            }
        </script>

        <script>
            function mostrarImagen(originalName,titulo,descripcion) {
                \$('#imagenAExpandir').attr('src','/images/Uploads/'+originalName);
                document.getElementById(\"textoTitulo\").innerHTML = titulo;
                document.getElementById(\"textoDescripcion\").innerHTML = descripcion;
            }
        </script>

        <script>
            function cerrarYPedir() {
                \$('#botonModal').attr(\"data-dismiss\",\"modal\");
                window.location.href = \"#reservation\"
            }
        </script>

    ";
        
        $__internal_09028ed3a9da449265c665a2214ab30df0a1894e2be2a6ef3dd66f5080dad996->leave($__internal_09028ed3a9da449265c665a2214ab30df0a1894e2be2a6ef3dd66f5080dad996_prof);

        
        $__internal_c99cc7a5844521f1c4dc4cef20e94d40a5dfd7accd6d448249335596005455ec->leave($__internal_c99cc7a5844521f1c4dc4cef20e94d40a5dfd7accd6d448249335596005455ec_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  848 => 483,  845 => 482,  836 => 481,  821 => 474,  815 => 472,  809 => 470,  807 => 469,  737 => 402,  729 => 396,  727 => 394,  719 => 388,  717 => 386,  713 => 385,  710 => 384,  708 => 381,  704 => 380,  656 => 335,  645 => 326,  643 => 323,  632 => 314,  630 => 310,  627 => 309,  625 => 307,  622 => 306,  620 => 304,  616 => 303,  613 => 302,  611 => 300,  607 => 299,  575 => 270,  552 => 249,  535 => 246,  532 => 245,  528 => 244,  525 => 243,  508 => 240,  505 => 239,  501 => 238,  498 => 237,  481 => 234,  478 => 233,  474 => 232,  471 => 231,  454 => 228,  451 => 227,  447 => 226,  402 => 184,  397 => 182,  390 => 178,  385 => 176,  374 => 168,  370 => 167,  365 => 165,  361 => 164,  336 => 141,  330 => 140,  328 => 139,  323 => 136,  314 => 135,  304 => 133,  301 => 132,  292 => 129,  289 => 128,  284 => 127,  281 => 126,  279 => 125,  276 => 124,  273 => 123,  264 => 120,  261 => 119,  256 => 118,  253 => 117,  251 => 116,  248 => 115,  245 => 114,  236 => 111,  233 => 110,  228 => 109,  225 => 108,  223 => 107,  220 => 106,  217 => 105,  208 => 102,  205 => 101,  200 => 100,  197 => 99,  195 => 98,  192 => 97,  189 => 96,  180 => 93,  177 => 92,  172 => 91,  169 => 90,  167 => 89,  164 => 88,  161 => 87,  152 => 84,  149 => 83,  144 => 82,  141 => 81,  139 => 80,  135 => 78,  127 => 43,  121 => 41,  115 => 39,  113 => 38,  92 => 19,  88 => 17,  82 => 15,  80 => 14,  70 => 6,  61 => 5,  43 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

    {% block title %}Sabores del Sur{% endblock %}

        {% block banner %}

            <button class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navHeaderCollapse\">
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>

            <div class=\"collapse navbar-collapse navHeaderCollapse\">
                {% if not is_granted('ROLE_USER') %}
                    <a class=\"animated bounceInLeft navbar-brand\" href=\"{{ path('security_login') }}\">Sabores del Sur</a>
                {% else %}
                    <a class=\"navbar-brand\">Sabores del Sur</a>
                {% endif %}
                <div class=\"container\">
                    <div class=\"col-lg-offset-6 col-md-offset-5 col-sm-offset-3\">
                        <ul class=\"nav navbar-nav main-nav clear\">
                            <li><a href=\"#top\" class=\"active color_animation\">Principal</a></li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle color_animation\" data-toggle=\"dropdown\">Productos <b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu color_animation\" style=\"background: #222\">
                                    <li><a class=\"color_animation\" href=\"#pricing\">Catálogo de Exquisiteses</a></li>
                                    <li><a class=\"color_animation\" href=\"#beer\">Nuestras Especialidades</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle color_animation\" data-toggle=\"dropdown\">Nosotros <b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu color_animation\" style=\"background: #222\">
                                    <li><a class=\"color_animation\" href=\"#story\">Sobre nosotros </a></li>
                                    <li><a class=\"color_animation\" href=\"#contact\">Contacto</a></li>
                                </ul>
                            </li>
                            <li><a href=\"#reservation\" class=\"color_animation\">Pedidos</a></li>
                            {% if is_granted('ROLE_USER') %}
                                <li><a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a></li>
                            {% else %}
                                <li><a class=\"color_animation\" href=\"{{ path('security_login') }}\">Ingresar</a></li>
                            {% endif %}
                        </ul>
                    </div>
                </div>

            </div>

{#            <div class=\"navbar-collapse collapse\" id=\".navbar-collapse\">
                <ul class=\"nav navbar-nav main-nav clear navbar-right\">
                    <li><a class=\"navactive color_animation\" href=\"#top\">Principal</a></li>
                    <li><a class=\"color_animation\" href=\"#story\">Conocenos</a></li>
                    <li><a class=\"color_animation\" href=\"#pricing\">Catálogo de exquisiteces</a></li>
                    <li><a class=\"color_animation\" href=\"#beer\">Especialidades</a></li>
                    <li><a class=\"color_animation\" href=\"#reservation\">Realizar Pedido</a></li>
                    <li><a class=\"color_animation\" href=\"#contact\">Contacto</a></li>
                </ul>
            </div>#}{#
#}{#

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class=\"collapse navbar-collapse\" id=\".navbar-collapse\">
                        <ul class=\"nav navbar-nav main-nav clear navbar-right \">
                            <li><a class=\"navactive color_animation\" href=\"#top\">Principal</a></li>
                            <li><a class=\"color_animation\" href=\"#story\">Conocenos</a></li>
                            <li><a class=\"color_animation\" href=\"#pricing\">Catálogo de exquisiteces</a></li>
                            <li><a class=\"color_animation\" href=\"#beer\">Especialidades</a></li>
                            <li><a class=\"color_animation\" href=\"#reservation\">Realizar Pedido</a></li>
                            <li><a class=\"color_animation\" href=\"#contact\">Contacto</a></li>
                            {% if is_granted('ROLE_USER') %}
                                <li><a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a></li>
                            {% else %}
                                <li><a class=\"color_animation\" href=\"{{ path('security_login') }}\">Iniciar Sesión</a></li>
                            {% endif %}
                        </ul>
                    </div>
#}

            <!-- ============ Mensaje de aviso exito al cargar imagen ============= -->
                        {% set flashbag_notices = app.session.flashbag.get('mensajeExito') %}
                        {% if flashbag_notices is not empty %}
                            {% for mensaje in flashbag_notices %}
                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                    {{ mensaje }}
                                </div>
                            {% endfor %}
                        {% endif %}
            <!-- ============ Mensaje de error limite de tamaño excedido ============= -->
                        {% set flashbag_notices = app.session.flashbag.get('mensajeLimiteTamaño') %}
                        {% if flashbag_notices is not empty %}
                            {% for mensaje in flashbag_notices %}
                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-danger\">
                                    {{ mensaje }}
                                </div>
                            {% endfor %}
                        {% endif %}
            <!-- ============ Mensaje de error tipo de archivo no reconocido ============= -->
                        {% set flashbag_notices = app.session.flashbag.get('mensajeTipoArchivo') %}
                        {% if flashbag_notices is not empty %}
                            {% for mensaje in flashbag_notices %}
                                <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-danger\">
                                    {{ mensaje }}
                                </div>
                            {% endfor %}
                        {% endif %}
            <!-- ============ Mensaje de consulta de pedido ============= -->
                    {% set flashbag_notices = app.session.flashbag.get('mensajeOrdenPedidoExito') %}
                    {% if flashbag_notices is not empty %}
                        {% for mensaje in flashbag_notices %}
                            <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                {{ mensaje }}
                            </div>
                        {% endfor %}
                    {% endif %}
            <!-- ============ Mensaje de contacto ============= -->
                    {% set flashbag_notices = app.session.flashbag.get('mensajeContactoExito') %}
                    {% if flashbag_notices is not empty %}
                        {% for mensaje in flashbag_notices %}
                            <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                                {{ mensaje }}
                            </div>
                        {% endfor %}
                    {% endif %}
            <!-- ============ Mensaje de aviso exito al cargar imagen ============= -->
            {% set flashbag_notices = app.session.flashbag.get('mensajeBorradoArchivo') %}
            {% if flashbag_notices is not empty %}
                {% for mensaje in flashbag_notices %}
                    <div class=\"col lg-10 col-md-10 col-sm-12 col-xs-12 alert alert-success\">
                        {{ mensaje }}
                    </div>
                {% endfor %}
            {% endif %}
        {% endblock %}

{% block body %}
        <div class=\"starter_container bg\">
            <div class=\"follow_container\">
                <div class=\"col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12\">
                    {% if is_granted('ROLE_USER') %}
                        <a class=\"animated bounceInLeft navbar-brand\" href=\"{{ path('admin_profile') }}\">Administrar</a>
                    {% endif %}<br><br><br><br>
                    <h2 class=\"animated swing navbar-brand-bigger\">Repostería</h2><br><br>
                    <hr class=\"animated bounceInLeft\">
                    <h2 class=\"animated bounceInDown white second-title\">\" La mejor en la ciudad \"</h2>
                    <hr class=\"animated bounceInRight\">
                </div>
            </div>
        </div>

        <!-- ============ About Us ============= -->

        <section id=\"story\" class=\"description_content scrollflow -slide-right -opacity\">
            <div class=\"text-content container\">
                <div class=\"col-md-6\">
                    <h1>Conócenos</h1>
                    <div class=\"fa fa-cutlery fa-2x\"></div>
                    <p class=\"desc-text\">Somos una familia bien dispuesta cuyo propósito es comenzar a emerger en este hermoso emprendimiento que busca convertir cada segundo en una verdadera aventura, no solo para el paladar de aquellos que lo prueben, sino también para la vida. Creemos que la cocina es un lenguaje mediante el cual se puede expresar armonia, creatividad, felicidad, poesia, magia, humor, provocación. Queda en ustedes que tipo emoción quieran generar! Sin más, los invito a ser parte de este sueño!</p>
                </div>

                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"hidden-xs col-md-6\">
                            <div class=\"img-section\">
                                <img src=\"{{ asset('images/Other/21192483_1679525915451816_4323066963082020934_n.jpg') }}\" width=\"250\" height=\"220\">
                                <img src=\"{{ asset('images/Other/21150099_1679438395460568_4188395351542253078_n.jpg') }}\" width=\"250\" height=\"220\">
                                <div class=\"img-section-space\"></div>
                                <img src=\"{{ asset('images/Other/21150057_1679527285451679_6146148214145008711_n.jpg') }}\" width=\"250\" height=\"220\">
                                <img src=\"{{ asset('images/Other/20180318_130317.jpg') }}\" width=\"250\" height=\"220\">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"hidden-lg hidden-md hidden-sm col-xs-6\">
                            <img src=\"{{ asset('images/Other/21192483_1679525915451816_4323066963082020934_n.jpg') }}\"  width=\"100%\">
                            <div class=\"img-section-space\"></div>
                            <img src=\"{{ asset('images/Other/21150099_1679438395460568_4188395351542253078_n.jpg') }}\" width=\"100%\">
                            <div class=\"img-section-space\"></div>
                        </div>
                        <div class=\"hidden-lg hidden-md hidden-sm col-md-6 col-xs-6\">
                            <img src=\"{{ asset('images/Other/21150057_1679527285451679_6146148214145008711_n.jpg') }}\"  width=\"97%\">
                            <div class=\"img-section-space\"></div>
                            <img src=\"{{ asset('images/Other/20180318_130317.jpg') }}\" width=\"97%\">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ============ Galeria de fotos ============= -->
        <section class=\"description_content\">
            <div class=\"fixed-bg-pricing\">
                <br><br>
                <h2 class=\"navbar-brand-bigger\">Precio único</h2>
            </div>
            <br><br>
        <div class=\"container\">
            <div class=\"row\">
                <hr>
                    <div id=\"pricing\" class=\"col-md-12\">
                        <h2 class=\"title\"><em>Mira nuestros productos y consulta tu pedido sin cargo!</em></h2>
                    </div>
                <hr>
                    <div class=\"col-md-12\">
                        <p style=\"text-align: center\" class=\"desc-text\"><em>Seleccione un elemento para realizar una consulta sin cargo alguno.</em></p><br>
                    </div>
                <div>
                    <div align=\"center\">
                        <button class=\"btn btn-success filter-button\" data-filter=\"all\">Todos</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"hdpe\">Tortas</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"sprinkle\">Tartas</button>
                        <br class=\"hidden-lg hidden-md\">
                        <br class=\"hidden-lg hidden-md\">
                        <button class=\"btn btn-info filter-button\" data-filter=\"spray\">Mesas Dulces</button>
                        <button class=\"btn btn-info filter-button\" data-filter=\"irrigation\">Variedades</button>
                    </div>
                </div>
                <div class=\"col-md-12\">
                    <div class=\"col-md-6 col-md-offset-3\">
                        <label  style=\"visibility: hidden\" id=\"codigoPedido\">Código de Pedido: </label>
                    </div>
                </div>

                <!-- Tortas -->
                    {% for torta in tortas %}
                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter hdpe\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ torta.getImage.getOriginalName }}' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('{{ torta.getImage.getOriginalName }}','{{ torta.tituloDescripcion }}','{{ torta.descripcion }}'), fijarCodigo('{{ torta.codigo }}')\" alt=\"Card image cap\">
                        </div>
                    {% endfor %}
                        <!-- Tartas -->
                    {% for tarta in tartas %}
                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter sprinkle\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ tarta.getImage.getOriginalName }}' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('{{ tarta.getImage.getOriginalName }}','{{ tarta.tituloDescripcion }}','{{ tarta.descripcion }}'), fijarCodigo('{{ tarta.codigo }}')\" alt=\"Card image cap\">
                        </div>
                    {% endfor %}
                        <!-- Mesas dulces -->
                    {% for mesaDulce in mesasDulces %}
                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter spray\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ mesaDulce.getImage.getOriginalName }}' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('{{ mesaDulce.getImage.getOriginalName }}','{{ mesaDulce.tituloDescripcion }}','{{ mesaDulce.descripcion }}'), fijarCodigo('{{ mesaDulce.codigo }}')\" alt=\"Card image cap\">
                        </div>
                    {% endfor %}
                        <!-- Variedades -->
                    {% for variedad in variedades %}
                        <div class=\"gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter irrigation\">
                            <img style=\"width: 320px; height: 220px;\" class=\"card-img-top imagenGaleria activarBoton\" src='/images/Uploads/{{ variedad.getImage.getOriginalName }}' data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"mostrarImagen('{{ variedad.getImage.getOriginalName }}','{{ variedad.tituloDescripcion }}','{{ variedad.descripcion }}'), fijarCodigo('{{ variedad.codigo }}')\" alt=\"Card image cap\">
                        </div>
                    {% endfor %}
            </div>
        </div>
        </section>

        <!-- ============ Nuestras Tortas  ============= -->
        <section id =\"beer\" class=\"description_content\">
            <div  class=\"fixed-bg-specialty\">
                <br><br>
                <h2 class=\"navbar-brand-bigger\">Nuestra especialidad</h2>
            </div>
            <div class=\"text-content container\">

                <div class=\"col-md-6 col-md-offset-1\">
                    <h1>Nuestras tortas</h1>
                    <div class=\"fa fa-birthday-cake fa-3x\"></div>
                        <p class=\"desc-text\">Todos nuestros bizcochuelos son caseros elaborados con ingredientes de la mejor calidad, por eso se ven los resultados al deleitar el paladar de nuestrs clientes.</p>
                        <br>
                        <p class=\"desc-text\">Te invitamos a probar nuestra exquisita Torta Du Soleil 2.0 y nos cuentes que te parece!</p>
                </div>
                <div class=\"col-md-5 col-\">
                    <div class=\"img-section\">
                        <img src=\"{{ asset('images/especialidad.jpeg') }}\" width=\"80%\">
                    </div>
                </div>
            </div>
            <br>
        </section>

        <!-- ============ Reservation  ============= -->

        <section class=\"description_content\">
            <div class=\"fixed-bg-order scrollflow -slide-bottom -opacity\">
                <br><br><br><br>
                <h2 class=\"navbar-brand-bigger-pedido\">Consulta por tu pedido!</h2>
            </div>
            <div class=\"text-content container\">
                <div class=\"inner contact\">
                    <!-- Form Area -->
                    <div id=\"reservation\" class=\"contact-form\">
                        <!-- Form -->
                            <hr>
                            <div class=\"row scrollflow -slide-bottom -opacity\">
                                <div class=\"col-md-12\">
                                    <h2 class=\"second-title\"> Completa tus datos para consultar por tu pedido!</h2>
                                </div>
                            </div>
                            <hr>
                                <div class=\"text-content container scrollflow -slide-top -opacity\">
                                    <div class=\"col-md-5\">
                                        <div>
                                            {{ form_start(formOrder) }}
                                            {{ form_row(formOrder.name, {
                                                'label': 'Nombre completo',
                                            }) }}
                                            {{ form_row(formOrder.email) }}
                                            {{ form_row(formOrder.phone, {
                                                'label': 'Teléfono',
                                            }) }}
                                            {{ form_row(formOrder.dateExpected, {
                                                'label': 'Fecha en la que necesito este producto',
                                            }) }}
                                            {{ form_row(formOrder.orderCode, {
                                                'label': false,
                                                'id': 'codPedido',
                                                'attr': {'readonly':'true'}
                                            }) }}
                                        </div>
                                    </div>
                                    <br class=\"hidden-xs\">
                                    <div class=\"col-md-6 col-md-offset-1\">
                                        <p class=\"text-info\">Ya casi! Sólo falta una descripción...</p>
                                        <div class=\"fa fa-thumbs-o-up fa-4x\"></div>
                                        <p class=\"desc-text\">Cuéntanos cada detalle que deseas en tu pedido, nosotros te responderemos al instante sobre nuestros precios y recomendaciones sin ningún compromiso de compra.</p>
                                        <br><br>
                                        {{ form_row(formOrder.descripcion, {
                                            'label': 'Descripción',
                                            'attr': {'placeholder': '¿Cómo decorarías este producto?Cuentanos... :)'}
                                        }) }}
                                        <div class=\"col-lg-offset-8\">
                                            <h6>(Cant. Pisos; Colores; Extras).</h6>
                                        </div>
                                    </div>
                                    <div class=\"col-md-10 col-lg-offset-1\" >
                                    <button onclick=\"location.href='#reservation';\" type=\"submit\" class=\"btn btn-success btn-lg btn-block\" formnovalidate>Consultar por este Pedido!</button>

                                    </div>
                                        {{ form_end(formOrder) }}
                                </div>
                            </div>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>

        <!-- ============ Social Section  ============= -->

        <section class=\"social_connect scrollflow -slide-top -opacity\">
            <div class=\"text-content container\">
                <div class=\"col-md-6\">
                    <span class=\"social_heading\">Seguinos</span>
                    <ul class=\"social_icons\">
                        <li><a class=\"icon-twitter color_animation\" href=\"#\" target=\"_blank\"></a></li>
                        <li><a class=\"icon-github color_animation\" href=\"#\" target=\"_blank\"></a></li>
                        <li><a class=\"fa fa-facebook color_animation\" href=\"https://www.facebook.com/taniasantossaboresdelsur/\" target=\"_blank\"></a></li>
                        <li><a class=\"icon-mail color_animation\" href=\"#\"></a></li>
                    </ul>
                </div>
                <div class=\"col-md-4\">
                    <span class=\"social_heading\">O Llamanos</span>
                    <span class=\"social_info\"><a class=\"color_animation\" href=\"tel:156-246-712\">(0351)156-246-712</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Contact Section  ============= -->
        <section class=\"description_content\">
            <div id=\"contact\" class=\"text-content container\">
                <div class=\"inner contact\">
                    <!-- Form Area -->
                    <div class=\"contact-form\">
                        <!-- Form -->
                        <hr>
                        <div class=\"row scrollflow -slide-bottom -opacity\">
                            <div class=\"col-md-12\">
                                <h2 class=\"second-title\"> Tu opinión nos interesa, dejanos un comentario!</h2>
                            </div>
                        </div>
                        <hr>
                        <div class=\" row text-content container scrollflow -slide-top -opacity\">
                            <div class=\"col-md-5\">
                                <div>
                                    {{ form_start(formContact) }}
                                    {{ form_row(formContact.name, {
                                        'label': 'Nombre completo',

                                    }) }}
                                    {{ form_row(formContact.email) }}
                                    {{ form_row(formContact.topic, {
                                        'label': 'Tema',
                                    }) }}
                                </div>
                            </div>
                            <br>
                            <div class=\"col-md-6 col-md-offset-1\">
                                <div class=\"fa fa-thumbs-o-up fa-4x\"></div>
                                {{ form_row(formContact.mensaje, {
                                    'label': 'Mensaje'
                                }) }}
                            </div>
                            <div class=\"col-md-10 col-lg-offset-1\" >
                                <button onclick=\"location.href='#contact';\" type=\"submit\" class=\"btn btn-success btn-lg btn-block\" formnovalidate>Enviar Mensaje!</button>
                                <br>
                            </div>
                            {{ form_end(formContact) }}
                        </div>
                    </div>
                </div><!-- End Contact Form Area -->
            </div><!-- End Inner -->
            </div>
        </section>


<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalTitle\" aria-hidden=\"true\">
<div class=\"modal-dialog\" role=\"document\">
<div class=\"modal-content\">
 <div class=\"modal-header\">
         <div class=\"row\">
             <div class=\"col-lg-11 col-md-10 col-sm-10 col-xs-9\">
                 <em><h5 class=\"modal-title\" id=\"myModalTitle\"><p style=\"font-size: 20px;\" id=\"textoTitulo\"></p></h5></em>
             </div>
             <div class=\"col-lg-1 col-md-2 col-sm-2 col-xs-3\">
                 <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                     <span aria-hidden=\"true\">&times;</span>
                 </button>
             </div>
         </div>
 </div>
 <div class=\"modal-body\">
     <div class=\"container\">
     <div class=\"row\">
         <div class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\">
             <em>
                 <p style=\"font-size: 13px;\" id=\"textoDescripcion\"></p>
                 <p style=\"font-size: 13px;\" class=\"desc-text\">Aclaración: Todos los rellenos descriptos pueden modificarse a gusto del cliente.</p>
             </em>
         </div>
     </div>
     <div class=\"row\">
         <div align=\"middle\" class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\" >
             <img id=\"imagenAExpandir\" style=\" width: 220px;\" alt=\"Card image cap\">
         </div>
     </div>
     </div>
 </div>
 <div class=\"modal-footer\">
     <div class=\"container\">
         <div class=\"row\">
             <div class=\"col-lg-6 col-md-7 col-sm-9 col-xs-12\">
                 <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cerrar</button>
                 <button type=\"button\" id=\"botonModal\" onclick=\"cerrarYPedir()\" class=\"btn btn-primary\">Esta es para mí, consultar!</button>
             </div>
         </div>
     </div>
 </div>
</div>
</div>
</div>

    <!-- ============ Footer Section  ============= -->
        <footer class=\"sub_footer\">
            <div class=\"container\">
                <div class=\"row\">
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-5\">
                            <p class=\"sub-footer-text text-center\">&copy; Sabores del Sur 2017</p>
                        </div>
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">
                            <p class=\"sub-footer-text text-center\">Volver al <a href=\"#top\">Inicio</a></p>
                        </div>
                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-3\">
                            {% if is_granted('ROLE_USER') %}
                                <a class=\"color_animation\" href=\"{{ path('security_logout') }}\">Cerrar Sesión</a></li>
                            {% else %}
                                <a class=\"color_animation\" href=\"{{ path('security_login') }}\">Iniciar Sesión</a>
                            {% endif %}
                    </div>
                </div>
            </div>
        </footer>

    {% endblock %}

    {% block javascripts %}

        {{ parent() }}

        <script>
            function fijarCodigo(codigo) {
                document.getElementById(\"codigoPedido\").innerHTML = \"Código de Pedido:\" + codigo;
                \$('#codPedido').val(codigo);
            }
        </script>

        <script>
            function mostrarImagen(originalName,titulo,descripcion) {
                \$('#imagenAExpandir').attr('src','/images/Uploads/'+originalName);
                document.getElementById(\"textoTitulo\").innerHTML = titulo;
                document.getElementById(\"textoDescripcion\").innerHTML = descripcion;
            }
        </script>

        <script>
            function cerrarYPedir() {
                \$('#botonModal').attr(\"data-dismiss\",\"modal\");
                window.location.href = \"#reservation\"
            }
        </script>

    {% endblock %}", "index/index.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/app/Resources/views/index/index.html.twig");
    }
}
