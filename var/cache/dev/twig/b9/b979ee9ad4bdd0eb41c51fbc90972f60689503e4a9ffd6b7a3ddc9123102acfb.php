<?php

/* base.html.twig */
class __TwigTemplate_3a01a3eff817aad8b8199d40d184e915074ec3246bfc164532d382222a5d40c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'banner' => array($this, 'block_banner'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_449e9f05f18132529483bb01c7e342e25f0f3f81b1cfc4f8eb6a436c00c35275 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_449e9f05f18132529483bb01c7e342e25f0f3f81b1cfc4f8eb6a436c00c35275->enter($__internal_449e9f05f18132529483bb01c7e342e25f0f3f81b1cfc4f8eb6a436c00c35275_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_c2e7ed7417a44246a4885d97202ac8906063b179aea4a08130b93b5a1fdcba79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2e7ed7417a44246a4885d97202ac8906063b179aea4a08130b93b5a1fdcba79->enter($__internal_c2e7ed7417a44246a4885d97202ac8906063b179aea4a08130b93b5a1fdcba79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 24
        echo "
<!--    Elemento Cargando   -->
        <style>
            .no-js #loader { display: none;  }
            .js #loader { display: block; position: absolute; left: 100px; top: 0; }
            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(images/Preloader_2.gif) center no-repeat #fff;
            }
        </style>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js\"></script>
        <script>
            \$(window).load(function() {
                // Animate loader off screen
                \$(\".se-pre-con\").fadeOut(\"slow\");
            });
        </script>

    </head>

    <body>
    <!--    Elemento Cargando   -->
    <div class=\"se-pre-con\"></div>

    ";
        // line 55
        echo "    <header id=\"top\" class=\"navbar navbar-inverse navbar-fixed-top\">
    <container>

        ";
        // line 58
        $this->displayBlock('banner', $context, $blocks);
        // line 61
        echo "
    </container>

    </header>



    ";
        // line 100
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 101
        echo "

        ";
        // line 103
        $this->displayBlock('javascripts', $context, $blocks);
        // line 104
        echo "
        <script type=\"text/javascript\" src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-1.10.2.js"), "html", null, true);
        echo "\" ></script>
        <script type=\"text/javascript\" src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.mixitup.min.js"), "html", null, true);
        echo "\" ></script>
        <script type=\"text/javascript\" src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\" ></script>
        <script type=\"text/javascript\" src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" ></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/eskju.jquery.scrollflow.min.js"), "html", null, true);
        echo "\" ></script>
        <script>
            // Bootstrap Datepicker URL: http://bootstrap-datepicker.readthedocs.io/en/latest/
            jQuery(document).ready(function () {
                \$('.js-datepicker').datepicker({
                    format: 'yyyy-mm-dd'
                });
            })
        </script>
    </body>
</html>
";
        
        $__internal_449e9f05f18132529483bb01c7e342e25f0f3f81b1cfc4f8eb6a436c00c35275->leave($__internal_449e9f05f18132529483bb01c7e342e25f0f3f81b1cfc4f8eb6a436c00c35275_prof);

        
        $__internal_c2e7ed7417a44246a4885d97202ac8906063b179aea4a08130b93b5a1fdcba79->leave($__internal_c2e7ed7417a44246a4885d97202ac8906063b179aea4a08130b93b5a1fdcba79_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c3a14a64e299aef273ee06e0351e0a58f0a39543ada9b181cb26b82d7bd32fdc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3a14a64e299aef273ee06e0351e0a58f0a39543ada9b181cb26b82d7bd32fdc->enter($__internal_c3a14a64e299aef273ee06e0351e0a58f0a39543ada9b181cb26b82d7bd32fdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_3e50a289d76c64547c0ff5239e757683f1a00f3b1dcf136ee6af3eb11f9ec612 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e50a289d76c64547c0ff5239e757683f1a00f3b1dcf136ee6af3eb11f9ec612->enter($__internal_3e50a289d76c64547c0ff5239e757683f1a00f3b1dcf136ee6af3eb11f9ec612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sabores del Sur";
        
        $__internal_3e50a289d76c64547c0ff5239e757683f1a00f3b1dcf136ee6af3eb11f9ec612->leave($__internal_3e50a289d76c64547c0ff5239e757683f1a00f3b1dcf136ee6af3eb11f9ec612_prof);

        
        $__internal_c3a14a64e299aef273ee06e0351e0a58f0a39543ada9b181cb26b82d7bd32fdc->leave($__internal_c3a14a64e299aef273ee06e0351e0a58f0a39543ada9b181cb26b82d7bd32fdc_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_15b6fa0a6003f8916764ee2c8362ef4b336ddabdd0de7adfaf21a4ed313b9384 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15b6fa0a6003f8916764ee2c8362ef4b336ddabdd0de7adfaf21a4ed313b9384->enter($__internal_15b6fa0a6003f8916764ee2c8362ef4b336ddabdd0de7adfaf21a4ed313b9384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_2d805b7c8f82eab87dc56cabffa91a5cf9355ec294c86f48860ba71c78365c33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d805b7c8f82eab87dc56cabffa91a5cf9355ec294c86f48860ba71c78365c33->enter($__internal_2d805b7c8f82eab87dc56cabffa91a5cf9355ec294c86f48860ba71c78365c33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/fontawesome/css/font-awesome.min.css"), "html", null, true);
        echo "\">
            <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/normalize.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" media=\"screen\" type=\"text/css\">
            <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
            <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style-portfolio.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/picto-foundry-food.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css\">
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css\">
            <link rel=\"icon\" href=\"favicon-1.ico\" type=\"image/x-icon\">
        ";
        
        $__internal_2d805b7c8f82eab87dc56cabffa91a5cf9355ec294c86f48860ba71c78365c33->leave($__internal_2d805b7c8f82eab87dc56cabffa91a5cf9355ec294c86f48860ba71c78365c33_prof);

        
        $__internal_15b6fa0a6003f8916764ee2c8362ef4b336ddabdd0de7adfaf21a4ed313b9384->leave($__internal_15b6fa0a6003f8916764ee2c8362ef4b336ddabdd0de7adfaf21a4ed313b9384_prof);

    }

    // line 58
    public function block_banner($context, array $blocks = array())
    {
        $__internal_a64c9aa2e75258509ea240fc26975d037e3a6be75bfbc4194f8d5c90ff426888 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a64c9aa2e75258509ea240fc26975d037e3a6be75bfbc4194f8d5c90ff426888->enter($__internal_a64c9aa2e75258509ea240fc26975d037e3a6be75bfbc4194f8d5c90ff426888_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        $__internal_7a963c38163a775539cdda068f672820d5d59822d6d409d5021f8c177780128d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a963c38163a775539cdda068f672820d5d59822d6d409d5021f8c177780128d->enter($__internal_7a963c38163a775539cdda068f672820d5d59822d6d409d5021f8c177780128d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banner"));

        // line 59
        echo "
        ";
        
        $__internal_7a963c38163a775539cdda068f672820d5d59822d6d409d5021f8c177780128d->leave($__internal_7a963c38163a775539cdda068f672820d5d59822d6d409d5021f8c177780128d_prof);

        
        $__internal_a64c9aa2e75258509ea240fc26975d037e3a6be75bfbc4194f8d5c90ff426888->leave($__internal_a64c9aa2e75258509ea240fc26975d037e3a6be75bfbc4194f8d5c90ff426888_prof);

    }

    // line 100
    public function block_body($context, array $blocks = array())
    {
        $__internal_1eb44706dd3170f3a601d3dd61281a43ec16e347e4e7179bf6df6d6173a136b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1eb44706dd3170f3a601d3dd61281a43ec16e347e4e7179bf6df6d6173a136b5->enter($__internal_1eb44706dd3170f3a601d3dd61281a43ec16e347e4e7179bf6df6d6173a136b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_098baac818237a1ba43b2aaa0b549bb70a04b2c2b2a58dd47f38013c052ffdbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_098baac818237a1ba43b2aaa0b549bb70a04b2c2b2a58dd47f38013c052ffdbe->enter($__internal_098baac818237a1ba43b2aaa0b549bb70a04b2c2b2a58dd47f38013c052ffdbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_098baac818237a1ba43b2aaa0b549bb70a04b2c2b2a58dd47f38013c052ffdbe->leave($__internal_098baac818237a1ba43b2aaa0b549bb70a04b2c2b2a58dd47f38013c052ffdbe_prof);

        
        $__internal_1eb44706dd3170f3a601d3dd61281a43ec16e347e4e7179bf6df6d6173a136b5->leave($__internal_1eb44706dd3170f3a601d3dd61281a43ec16e347e4e7179bf6df6d6173a136b5_prof);

    }

    // line 103
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1461efa4ae7857199f473a0d53623162f3c6d3e67916aa8236d231ed8ac8a158 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1461efa4ae7857199f473a0d53623162f3c6d3e67916aa8236d231ed8ac8a158->enter($__internal_1461efa4ae7857199f473a0d53623162f3c6d3e67916aa8236d231ed8ac8a158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_46ee13afb1c48170eab0d578d67acf994e8c1208987a1a6555dee6ebbcbeecd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46ee13afb1c48170eab0d578d67acf994e8c1208987a1a6555dee6ebbcbeecd9->enter($__internal_46ee13afb1c48170eab0d578d67acf994e8c1208987a1a6555dee6ebbcbeecd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_46ee13afb1c48170eab0d578d67acf994e8c1208987a1a6555dee6ebbcbeecd9->leave($__internal_46ee13afb1c48170eab0d578d67acf994e8c1208987a1a6555dee6ebbcbeecd9_prof);

        
        $__internal_1461efa4ae7857199f473a0d53623162f3c6d3e67916aa8236d231ed8ac8a158->leave($__internal_1461efa4ae7857199f473a0d53623162f3c6d3e67916aa8236d231ed8ac8a158_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 103,  243 => 100,  232 => 59,  223 => 58,  208 => 19,  203 => 17,  199 => 16,  195 => 15,  191 => 14,  185 => 11,  181 => 10,  177 => 9,  173 => 8,  168 => 7,  159 => 6,  141 => 5,  119 => 110,  114 => 108,  110 => 107,  106 => 106,  102 => 105,  99 => 104,  97 => 103,  93 => 101,  90 => 100,  81 => 61,  79 => 58,  74 => 55,  42 => 24,  40 => 6,  36 => 5,  30 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Sabores del Sur{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('vendor/fontawesome/css/font-awesome.min.css') }}\">
            <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('css/normalize.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/main.css') }}\" media=\"screen\" type=\"text/css\">
            <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/style-portfolio.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/picto-foundry-food.css') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('css/jquery-ui.css') }}\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <link href=\"{{ asset('css/font-awesome.min.css') }}\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css\">
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css\">
            <link rel=\"icon\" href=\"favicon-1.ico\" type=\"image/x-icon\">
        {% endblock %}

<!--    Elemento Cargando   -->
        <style>
            .no-js #loader { display: none;  }
            .js #loader { display: block; position: absolute; left: 100px; top: 0; }
            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(images/Preloader_2.gif) center no-repeat #fff;
            }
        </style>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js\"></script>
        <script>
            \$(window).load(function() {
                // Animate loader off screen
                \$(\".se-pre-con\").fadeOut(\"slow\");
            });
        </script>

    </head>

    <body>
    <!--    Elemento Cargando   -->
    <div class=\"se-pre-con\"></div>

    {#asd#}
    <header id=\"top\" class=\"navbar navbar-inverse navbar-fixed-top\">
    <container>

        {% block banner %}

        {% endblock %}

    </container>

    </header>



    {#asd#}
{#
    <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-3 col-md-4 col-sm-5 col-xs-8\">
                        <!--Pregunto si esta logueado o no para iniciar o cerrar sesion-->
                        {% if not is_granted('ROLE_USER') %}
                            <a class=\"animated bounceInLeft navbar-brand\" href=\"{{ path('security_login') }}\">Sabores del Sur</a>
                        {% else %}
                            <a class=\"navbar-brand\">Sabores del Sur</a>
                        {% endif %}
                </div>
                <div class=\"col-lg-3 col-md-4 col-sm-5 col-xs-4 navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                            <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                </div>

                <div class=\"col-lg-9 col-md-8 col-sm-7 col-xs-7\">
                {% block banner %}

                {% endblock %}
                </div>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </nav>
#}
        {% block body %}{% endblock %}


        {% block javascripts %}{% endblock %}

        <script type=\"text/javascript\" src=\"{{ asset('js/jquery-1.10.2.js') }}\" ></script>
        <script type=\"text/javascript\" src=\"{{ asset('js/jquery.mixitup.min.js') }}\" ></script>
        <script type=\"text/javascript\" src=\"{{ asset('js/main.js') }}\" ></script>
        <script type=\"text/javascript\" src=\"{{ asset('js/bootstrap.min.js') }}\" ></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('js/eskju.jquery.scrollflow.min.js') }}\" ></script>
        <script>
            // Bootstrap Datepicker URL: http://bootstrap-datepicker.readthedocs.io/en/latest/
            jQuery(document).ready(function () {
                \$('.js-datepicker').datepicker({
                    format: 'yyyy-mm-dd'
                });
            })
        </script>
    </body>
</html>
", "base.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/app/Resources/views/base.html.twig");
    }
}
