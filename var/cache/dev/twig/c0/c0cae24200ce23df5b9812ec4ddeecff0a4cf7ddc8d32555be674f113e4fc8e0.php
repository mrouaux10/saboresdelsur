<?php

/* VichUploaderBundle:Form:fields.html.twig */
class __TwigTemplate_c054f2c2bdcb6949951aca71bb7b8f6b5ba9eba992feab3efbfed52d8f7b729d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'vich_file_row' => array($this, 'block_vich_file_row'),
            'vich_file_widget' => array($this, 'block_vich_file_widget'),
            'vich_image_row' => array($this, 'block_vich_image_row'),
            'vich_image_widget' => array($this, 'block_vich_image_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d89f3361e1b6e11567889da7e832ef43c7167d1c5beaec8fa644a82f21ffe9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d89f3361e1b6e11567889da7e832ef43c7167d1c5beaec8fa644a82f21ffe9c->enter($__internal_5d89f3361e1b6e11567889da7e832ef43c7167d1c5beaec8fa644a82f21ffe9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "VichUploaderBundle:Form:fields.html.twig"));

        $__internal_8d272b5a6791ff315c4eeafb7bede76efa899a059c63c9b4e70b9b7a2df741ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d272b5a6791ff315c4eeafb7bede76efa899a059c63c9b4e70b9b7a2df741ed->enter($__internal_8d272b5a6791ff315c4eeafb7bede76efa899a059c63c9b4e70b9b7a2df741ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "VichUploaderBundle:Form:fields.html.twig"));

        // line 1
        $this->displayBlock('vich_file_row', $context, $blocks);
        // line 5
        echo "
";
        // line 6
        $this->displayBlock('vich_file_widget', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('vich_image_row', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('vich_image_widget', $context, $blocks);
        
        $__internal_5d89f3361e1b6e11567889da7e832ef43c7167d1c5beaec8fa644a82f21ffe9c->leave($__internal_5d89f3361e1b6e11567889da7e832ef43c7167d1c5beaec8fa644a82f21ffe9c_prof);

        
        $__internal_8d272b5a6791ff315c4eeafb7bede76efa899a059c63c9b4e70b9b7a2df741ed->leave($__internal_8d272b5a6791ff315c4eeafb7bede76efa899a059c63c9b4e70b9b7a2df741ed_prof);

    }

    // line 1
    public function block_vich_file_row($context, array $blocks = array())
    {
        $__internal_fb1ea9b8fccd0a02c5dd7dad24ee0febe73d443920059089877dd626d66edf71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb1ea9b8fccd0a02c5dd7dad24ee0febe73d443920059089877dd626d66edf71->enter($__internal_fb1ea9b8fccd0a02c5dd7dad24ee0febe73d443920059089877dd626d66edf71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_row"));

        $__internal_2779fbb0c89bd5125e611b65bf2c58213c1d2f065de9d36cb3e2e8e3e2e65aaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2779fbb0c89bd5125e611b65bf2c58213c1d2f065de9d36cb3e2e8e3e2e65aaf->enter($__internal_2779fbb0c89bd5125e611b65bf2c58213c1d2f065de9d36cb3e2e8e3e2e65aaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_row"));

        // line 2
        $context["force_error"] = true;
        // line 3
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_2779fbb0c89bd5125e611b65bf2c58213c1d2f065de9d36cb3e2e8e3e2e65aaf->leave($__internal_2779fbb0c89bd5125e611b65bf2c58213c1d2f065de9d36cb3e2e8e3e2e65aaf_prof);

        
        $__internal_fb1ea9b8fccd0a02c5dd7dad24ee0febe73d443920059089877dd626d66edf71->leave($__internal_fb1ea9b8fccd0a02c5dd7dad24ee0febe73d443920059089877dd626d66edf71_prof);

    }

    // line 6
    public function block_vich_file_widget($context, array $blocks = array())
    {
        $__internal_4c9106793870175148c42852360ec8062adf9b2f9adda45e145aac9697f8b762 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c9106793870175148c42852360ec8062adf9b2f9adda45e145aac9697f8b762->enter($__internal_4c9106793870175148c42852360ec8062adf9b2f9adda45e145aac9697f8b762_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_widget"));

        $__internal_58de29e06dd17d2a6dca5cc9d02eefe7541e24f8bbe2cca16c9bc7461a463a89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58de29e06dd17d2a6dca5cc9d02eefe7541e24f8bbe2cca16c9bc7461a463a89->enter($__internal_58de29e06dd17d2a6dca5cc9d02eefe7541e24f8bbe2cca16c9bc7461a463a89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_widget"));

        // line 7
        echo "    ";
        ob_start();
        // line 8
        echo "        <div class=\"vich-file\">
            ";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), "file", array()), 'widget');
        echo "
            ";
        // line 10
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "delete", array(), "any", true, true)) {
            // line 11
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), "delete", array()), 'row');
            echo "
            ";
        }
        // line 13
        echo "
            ";
        // line 14
        if ((isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 14, $this->getSourceContext()); })())) {
            // line 15
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 15, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ((((isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 15, $this->getSourceContext()); })()) === false)) ? ((isset($context["download_label"]) || array_key_exists("download_label", $context) ? $context["download_label"] : (function () { throw new Twig_Error_Runtime('Variable "download_label" does not exist.', 15, $this->getSourceContext()); })())) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["download_label"]) || array_key_exists("download_label", $context) ? $context["download_label"] : (function () { throw new Twig_Error_Runtime('Variable "download_label" does not exist.', 15, $this->getSourceContext()); })()), array(), (isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 15, $this->getSourceContext()); })())))), "html", null, true);
            echo "</a>
            ";
        }
        // line 17
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_58de29e06dd17d2a6dca5cc9d02eefe7541e24f8bbe2cca16c9bc7461a463a89->leave($__internal_58de29e06dd17d2a6dca5cc9d02eefe7541e24f8bbe2cca16c9bc7461a463a89_prof);

        
        $__internal_4c9106793870175148c42852360ec8062adf9b2f9adda45e145aac9697f8b762->leave($__internal_4c9106793870175148c42852360ec8062adf9b2f9adda45e145aac9697f8b762_prof);

    }

    // line 21
    public function block_vich_image_row($context, array $blocks = array())
    {
        $__internal_f7d395ed7a8e8688b0aa4c0f7ebe1b917ac10e3fb3eddd3b868805d334dc6bcc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7d395ed7a8e8688b0aa4c0f7ebe1b917ac10e3fb3eddd3b868805d334dc6bcc->enter($__internal_f7d395ed7a8e8688b0aa4c0f7ebe1b917ac10e3fb3eddd3b868805d334dc6bcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_row"));

        $__internal_39281a437e35959fcf7d55da0182bd9cddee2e4cc628bcc25bfcb006981cc7c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39281a437e35959fcf7d55da0182bd9cddee2e4cc628bcc25bfcb006981cc7c7->enter($__internal_39281a437e35959fcf7d55da0182bd9cddee2e4cc628bcc25bfcb006981cc7c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_row"));

        // line 22
        $context["force_error"] = true;
        // line 23
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_39281a437e35959fcf7d55da0182bd9cddee2e4cc628bcc25bfcb006981cc7c7->leave($__internal_39281a437e35959fcf7d55da0182bd9cddee2e4cc628bcc25bfcb006981cc7c7_prof);

        
        $__internal_f7d395ed7a8e8688b0aa4c0f7ebe1b917ac10e3fb3eddd3b868805d334dc6bcc->leave($__internal_f7d395ed7a8e8688b0aa4c0f7ebe1b917ac10e3fb3eddd3b868805d334dc6bcc_prof);

    }

    // line 26
    public function block_vich_image_widget($context, array $blocks = array())
    {
        $__internal_aff76f5145bc6dc01e803051c5f1df5281aab05975f672256c45451f67556abe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aff76f5145bc6dc01e803051c5f1df5281aab05975f672256c45451f67556abe->enter($__internal_aff76f5145bc6dc01e803051c5f1df5281aab05975f672256c45451f67556abe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_widget"));

        $__internal_4b1a056e33e4e4509d9630817a31e18bc6f1a43b7d0dc3c9b2988e39cdab9601 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b1a056e33e4e4509d9630817a31e18bc6f1a43b7d0dc3c9b2988e39cdab9601->enter($__internal_4b1a056e33e4e4509d9630817a31e18bc6f1a43b7d0dc3c9b2988e39cdab9601_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_widget"));

        // line 27
        echo "    ";
        ob_start();
        // line 28
        echo "        <div class=\"vich-image\">
            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 29, $this->getSourceContext()); })()), "file", array()), 'widget');
        echo "
            ";
        // line 30
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "delete", array(), "any", true, true)) {
            // line 31
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 31, $this->getSourceContext()); })()), "delete", array()), 'row');
            echo "
            ";
        }
        // line 33
        echo "
            ";
        // line 34
        if ((isset($context["image_uri"]) || array_key_exists("image_uri", $context) ? $context["image_uri"] : (function () { throw new Twig_Error_Runtime('Variable "image_uri" does not exist.', 34, $this->getSourceContext()); })())) {
            // line 35
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["image_uri"]) || array_key_exists("image_uri", $context) ? $context["image_uri"] : (function () { throw new Twig_Error_Runtime('Variable "image_uri" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["image_uri"]) || array_key_exists("image_uri", $context) ? $context["image_uri"] : (function () { throw new Twig_Error_Runtime('Variable "image_uri" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
            echo "\" alt=\"\" /></a>
            ";
        }
        // line 37
        echo "            ";
        if ((isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 37, $this->getSourceContext()); })())) {
            // line 38
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 38, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ((((isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 38, $this->getSourceContext()); })()) === false)) ? ((isset($context["download_label"]) || array_key_exists("download_label", $context) ? $context["download_label"] : (function () { throw new Twig_Error_Runtime('Variable "download_label" does not exist.', 38, $this->getSourceContext()); })())) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["download_label"]) || array_key_exists("download_label", $context) ? $context["download_label"] : (function () { throw new Twig_Error_Runtime('Variable "download_label" does not exist.', 38, $this->getSourceContext()); })()), array(), (isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 38, $this->getSourceContext()); })())))), "html", null, true);
            echo "</a>
            ";
        }
        // line 40
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_4b1a056e33e4e4509d9630817a31e18bc6f1a43b7d0dc3c9b2988e39cdab9601->leave($__internal_4b1a056e33e4e4509d9630817a31e18bc6f1a43b7d0dc3c9b2988e39cdab9601_prof);

        
        $__internal_aff76f5145bc6dc01e803051c5f1df5281aab05975f672256c45451f67556abe->leave($__internal_aff76f5145bc6dc01e803051c5f1df5281aab05975f672256c45451f67556abe_prof);

    }

    public function getTemplateName()
    {
        return "VichUploaderBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  199 => 40,  191 => 38,  188 => 37,  180 => 35,  178 => 34,  175 => 33,  169 => 31,  167 => 30,  163 => 29,  160 => 28,  157 => 27,  148 => 26,  138 => 23,  136 => 22,  127 => 21,  115 => 17,  107 => 15,  105 => 14,  102 => 13,  96 => 11,  94 => 10,  90 => 9,  87 => 8,  84 => 7,  75 => 6,  65 => 3,  63 => 2,  54 => 1,  44 => 26,  41 => 25,  39 => 21,  36 => 20,  34 => 6,  31 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block vich_file_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock %}

{% block vich_file_widget %}
    {% spaceless %}
        <div class=\"vich-file\">
            {{ form_widget(form.file) }}
            {% if form.delete is defined %}
                {{ form_row(form.delete) }}
            {% endif %}

            {% if download_uri %}
                <a href=\"{{ download_uri }}\">{{ translation_domain is same as(false) ? download_label : download_label|trans({}, translation_domain) }}</a>
            {% endif %}
        </div>
    {% endspaceless %}
{% endblock %}

{% block vich_image_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock %}

{% block vich_image_widget %}
    {% spaceless %}
        <div class=\"vich-image\">
            {{ form_widget(form.file) }}
            {% if form.delete is defined %}
                {{ form_row(form.delete) }}
            {% endif %}

            {% if image_uri %}
                <a href=\"{{ image_uri }}\"><img src=\"{{ image_uri }}\" alt=\"\" /></a>
            {% endif %}
            {% if download_uri %}
                <a href=\"{{ download_uri }}\">{{ translation_domain is same as(false) ? download_label : download_label|trans({}, translation_domain) }}</a>
            {% endif %}
        </div>
    {% endspaceless %}
{% endblock %}
", "VichUploaderBundle:Form:fields.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/vich/uploader-bundle/Resources/views/Form/fields.html.twig");
    }
}
