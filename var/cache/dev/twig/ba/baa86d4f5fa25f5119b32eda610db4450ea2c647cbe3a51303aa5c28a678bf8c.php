<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_df422a50fbf3f194de697c369e0daae0e14bf31119859dc12097259454c03502 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_520e818b4a8d1479fb09d09966814e34952fe668fb8be2c862856d5a5e21ee50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_520e818b4a8d1479fb09d09966814e34952fe668fb8be2c862856d5a5e21ee50->enter($__internal_520e818b4a8d1479fb09d09966814e34952fe668fb8be2c862856d5a5e21ee50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_cd5887708be3cabc274405f16fc709ab0e7ad8b8976e221da53e9685be711426 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd5887708be3cabc274405f16fc709ab0e7ad8b8976e221da53e9685be711426->enter($__internal_cd5887708be3cabc274405f16fc709ab0e7ad8b8976e221da53e9685be711426_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_520e818b4a8d1479fb09d09966814e34952fe668fb8be2c862856d5a5e21ee50->leave($__internal_520e818b4a8d1479fb09d09966814e34952fe668fb8be2c862856d5a5e21ee50_prof);

        
        $__internal_cd5887708be3cabc274405f16fc709ab0e7ad8b8976e221da53e9685be711426->leave($__internal_cd5887708be3cabc274405f16fc709ab0e7ad8b8976e221da53e9685be711426_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_8f740fdbe6bca777c9fec57800e8eb33d933d5a27e804ad41d2931a3ee58c7e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f740fdbe6bca777c9fec57800e8eb33d933d5a27e804ad41d2931a3ee58c7e5->enter($__internal_8f740fdbe6bca777c9fec57800e8eb33d933d5a27e804ad41d2931a3ee58c7e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_654753a093778787c120656c7add0462de78e2da3ece60e2b88395d3615d185b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_654753a093778787c120656c7add0462de78e2da3ece60e2b88395d3615d185b->enter($__internal_654753a093778787c120656c7add0462de78e2da3ece60e2b88395d3615d185b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_654753a093778787c120656c7add0462de78e2da3ece60e2b88395d3615d185b->leave($__internal_654753a093778787c120656c7add0462de78e2da3ece60e2b88395d3615d185b_prof);

        
        $__internal_8f740fdbe6bca777c9fec57800e8eb33d933d5a27e804ad41d2931a3ee58c7e5->leave($__internal_8f740fdbe6bca777c9fec57800e8eb33d933d5a27e804ad41d2931a3ee58c7e5_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_53775e73f5985b74626be34c5c8203733a13f1b5d8afdc20a6b011f91d937359 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53775e73f5985b74626be34c5c8203733a13f1b5d8afdc20a6b011f91d937359->enter($__internal_53775e73f5985b74626be34c5c8203733a13f1b5d8afdc20a6b011f91d937359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_5971df8220b0043d9083222f67e78a972a292f986e628e5905286f9fbba8e26d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5971df8220b0043d9083222f67e78a972a292f986e628e5905286f9fbba8e26d->enter($__internal_5971df8220b0043d9083222f67e78a972a292f986e628e5905286f9fbba8e26d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_5971df8220b0043d9083222f67e78a972a292f986e628e5905286f9fbba8e26d->leave($__internal_5971df8220b0043d9083222f67e78a972a292f986e628e5905286f9fbba8e26d_prof);

        
        $__internal_53775e73f5985b74626be34c5c8203733a13f1b5d8afdc20a6b011f91d937359->leave($__internal_53775e73f5985b74626be34c5c8203733a13f1b5d8afdc20a6b011f91d937359_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea998b73bb486aa21e6ea9093ec50d35f7c17314bb7ef3e11bd078fbac1baefa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea998b73bb486aa21e6ea9093ec50d35f7c17314bb7ef3e11bd078fbac1baefa->enter($__internal_ea998b73bb486aa21e6ea9093ec50d35f7c17314bb7ef3e11bd078fbac1baefa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6eccbd18af184acb994277f52f9bab23869e909fa9a72fdaa106860798d64cb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6eccbd18af184acb994277f52f9bab23869e909fa9a72fdaa106860798d64cb8->enter($__internal_6eccbd18af184acb994277f52f9bab23869e909fa9a72fdaa106860798d64cb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6eccbd18af184acb994277f52f9bab23869e909fa9a72fdaa106860798d64cb8->leave($__internal_6eccbd18af184acb994277f52f9bab23869e909fa9a72fdaa106860798d64cb8_prof);

        
        $__internal_ea998b73bb486aa21e6ea9093ec50d35f7c17314bb7ef3e11bd078fbac1baefa->leave($__internal_ea998b73bb486aa21e6ea9093ec50d35f7c17314bb7ef3e11bd078fbac1baefa_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
