<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_dda7d1795e99423318dc1c7ef46f0fe82ff75e95f3507f8cd360f8b9e82a09f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a4951fe2841583520aeebf7ad05cde982939a949a361a571a99271c2ffc547c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a4951fe2841583520aeebf7ad05cde982939a949a361a571a99271c2ffc547c->enter($__internal_1a4951fe2841583520aeebf7ad05cde982939a949a361a571a99271c2ffc547c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_2e9b75895ddfa75fe9d02fd637c07584ea675f9dd2abfbd5ccff1b582e708f67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e9b75895ddfa75fe9d02fd637c07584ea675f9dd2abfbd5ccff1b582e708f67->enter($__internal_2e9b75895ddfa75fe9d02fd637c07584ea675f9dd2abfbd5ccff1b582e708f67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a4951fe2841583520aeebf7ad05cde982939a949a361a571a99271c2ffc547c->leave($__internal_1a4951fe2841583520aeebf7ad05cde982939a949a361a571a99271c2ffc547c_prof);

        
        $__internal_2e9b75895ddfa75fe9d02fd637c07584ea675f9dd2abfbd5ccff1b582e708f67->leave($__internal_2e9b75895ddfa75fe9d02fd637c07584ea675f9dd2abfbd5ccff1b582e708f67_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1ef06466aef4ce92307f133660dc06a4215ca3c27405ed541ab2e505774ca5bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ef06466aef4ce92307f133660dc06a4215ca3c27405ed541ab2e505774ca5bb->enter($__internal_1ef06466aef4ce92307f133660dc06a4215ca3c27405ed541ab2e505774ca5bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_8b8341c77608532bb5dfb69390c3f61b9183754a0c4a547516228c1e3efbaf63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b8341c77608532bb5dfb69390c3f61b9183754a0c4a547516228c1e3efbaf63->enter($__internal_8b8341c77608532bb5dfb69390c3f61b9183754a0c4a547516228c1e3efbaf63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_8b8341c77608532bb5dfb69390c3f61b9183754a0c4a547516228c1e3efbaf63->leave($__internal_8b8341c77608532bb5dfb69390c3f61b9183754a0c4a547516228c1e3efbaf63_prof);

        
        $__internal_1ef06466aef4ce92307f133660dc06a4215ca3c27405ed541ab2e505774ca5bb->leave($__internal_1ef06466aef4ce92307f133660dc06a4215ca3c27405ed541ab2e505774ca5bb_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f7b1040744f9fd7e2690e850053b5b6b595faa623385c0344e02e126c6198b22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7b1040744f9fd7e2690e850053b5b6b595faa623385c0344e02e126c6198b22->enter($__internal_f7b1040744f9fd7e2690e850053b5b6b595faa623385c0344e02e126c6198b22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a5c5719a2db7c89eea806728c7d15999595dab82e7e69163071bd4583d5a2e01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5c5719a2db7c89eea806728c7d15999595dab82e7e69163071bd4583d5a2e01->enter($__internal_a5c5719a2db7c89eea806728c7d15999595dab82e7e69163071bd4583d5a2e01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_a5c5719a2db7c89eea806728c7d15999595dab82e7e69163071bd4583d5a2e01->leave($__internal_a5c5719a2db7c89eea806728c7d15999595dab82e7e69163071bd4583d5a2e01_prof);

        
        $__internal_f7b1040744f9fd7e2690e850053b5b6b595faa623385c0344e02e126c6198b22->leave($__internal_f7b1040744f9fd7e2690e850053b5b6b595faa623385c0344e02e126c6198b22_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_08b6b73e6c7432c604c3a428212ba21cec3c98a6c42572394b18084af5376145 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08b6b73e6c7432c604c3a428212ba21cec3c98a6c42572394b18084af5376145->enter($__internal_08b6b73e6c7432c604c3a428212ba21cec3c98a6c42572394b18084af5376145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_018484645cbde6646c3c4cbca6d99a0dc3add95a586653fb936e560ad527f383 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_018484645cbde6646c3c4cbca6d99a0dc3add95a586653fb936e560ad527f383->enter($__internal_018484645cbde6646c3c4cbca6d99a0dc3add95a586653fb936e560ad527f383_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_018484645cbde6646c3c4cbca6d99a0dc3add95a586653fb936e560ad527f383->leave($__internal_018484645cbde6646c3c4cbca6d99a0dc3add95a586653fb936e560ad527f383_prof);

        
        $__internal_08b6b73e6c7432c604c3a428212ba21cec3c98a6c42572394b18084af5376145->leave($__internal_08b6b73e6c7432c604c3a428212ba21cec3c98a6c42572394b18084af5376145_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
