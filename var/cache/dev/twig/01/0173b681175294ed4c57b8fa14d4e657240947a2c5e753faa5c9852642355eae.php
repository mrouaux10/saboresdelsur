<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_2f138cf6709803be974f6307aa0fc155c24202ae7a87f5c2c3053c7c69f25f75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a8a0550b9a35d63e420ca5b634734045b83e8c444472279bbf6045550d21a77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a8a0550b9a35d63e420ca5b634734045b83e8c444472279bbf6045550d21a77->enter($__internal_7a8a0550b9a35d63e420ca5b634734045b83e8c444472279bbf6045550d21a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_dabdf44dd6f6d1dba18914ccef787c20443e3d66bc00dcba154a0631c560f3ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dabdf44dd6f6d1dba18914ccef787c20443e3d66bc00dcba154a0631c560f3ed->enter($__internal_dabdf44dd6f6d1dba18914ccef787c20443e3d66bc00dcba154a0631c560f3ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a8a0550b9a35d63e420ca5b634734045b83e8c444472279bbf6045550d21a77->leave($__internal_7a8a0550b9a35d63e420ca5b634734045b83e8c444472279bbf6045550d21a77_prof);

        
        $__internal_dabdf44dd6f6d1dba18914ccef787c20443e3d66bc00dcba154a0631c560f3ed->leave($__internal_dabdf44dd6f6d1dba18914ccef787c20443e3d66bc00dcba154a0631c560f3ed_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b4e6bbc1188128a02072ed42754179786f2e54fc8c06852ec760d0dcc347358f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4e6bbc1188128a02072ed42754179786f2e54fc8c06852ec760d0dcc347358f->enter($__internal_b4e6bbc1188128a02072ed42754179786f2e54fc8c06852ec760d0dcc347358f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_0c56a6d175b0afb3f2832c6276246c70ea70453da4df0be53340eaab89c3e486 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c56a6d175b0afb3f2832c6276246c70ea70453da4df0be53340eaab89c3e486->enter($__internal_0c56a6d175b0afb3f2832c6276246c70ea70453da4df0be53340eaab89c3e486_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_0c56a6d175b0afb3f2832c6276246c70ea70453da4df0be53340eaab89c3e486->leave($__internal_0c56a6d175b0afb3f2832c6276246c70ea70453da4df0be53340eaab89c3e486_prof);

        
        $__internal_b4e6bbc1188128a02072ed42754179786f2e54fc8c06852ec760d0dcc347358f->leave($__internal_b4e6bbc1188128a02072ed42754179786f2e54fc8c06852ec760d0dcc347358f_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_eaafb782ea0781c0e8cdb04006c880c5cd569585bf720e737e797a48acf83f02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaafb782ea0781c0e8cdb04006c880c5cd569585bf720e737e797a48acf83f02->enter($__internal_eaafb782ea0781c0e8cdb04006c880c5cd569585bf720e737e797a48acf83f02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_71cb4373b029366f411aa7d22fbe06563d9f732940d047b6f501cdbe389ea3dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71cb4373b029366f411aa7d22fbe06563d9f732940d047b6f501cdbe389ea3dd->enter($__internal_71cb4373b029366f411aa7d22fbe06563d9f732940d047b6f501cdbe389ea3dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_71cb4373b029366f411aa7d22fbe06563d9f732940d047b6f501cdbe389ea3dd->leave($__internal_71cb4373b029366f411aa7d22fbe06563d9f732940d047b6f501cdbe389ea3dd_prof);

        
        $__internal_eaafb782ea0781c0e8cdb04006c880c5cd569585bf720e737e797a48acf83f02->leave($__internal_eaafb782ea0781c0e8cdb04006c880c5cd569585bf720e737e797a48acf83f02_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_0956a1909ebb3ca17ff13f66843717d8fbf7406b7b51e83577092c46f35ed02a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0956a1909ebb3ca17ff13f66843717d8fbf7406b7b51e83577092c46f35ed02a->enter($__internal_0956a1909ebb3ca17ff13f66843717d8fbf7406b7b51e83577092c46f35ed02a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_336626e7b349f6aaa5830d7ce0dd7d953fbd59dc61c037da2734af020dc906e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_336626e7b349f6aaa5830d7ce0dd7d953fbd59dc61c037da2734af020dc906e6->enter($__internal_336626e7b349f6aaa5830d7ce0dd7d953fbd59dc61c037da2734af020dc906e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_336626e7b349f6aaa5830d7ce0dd7d953fbd59dc61c037da2734af020dc906e6->leave($__internal_336626e7b349f6aaa5830d7ce0dd7d953fbd59dc61c037da2734af020dc906e6_prof);

        
        $__internal_0956a1909ebb3ca17ff13f66843717d8fbf7406b7b51e83577092c46f35ed02a->leave($__internal_0956a1909ebb3ca17ff13f66843717d8fbf7406b7b51e83577092c46f35ed02a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/mrouaux/Documentos/Proyectos Symfony/saboresdelsur/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
